###############################################################################
#     Makefile for HOMREA
#     date: 31.05.2005
#     author: U. Maas, R. Stauch
#
#------------------------------------------------------------------------------
#     WARNING!
#	  Before using "make", call "./configure"
#     All configuration, such architecture additional flags should be set up
#        through ./configure
#     Please refer to ./configure --help or http://www.gnu.org/software/autoconf
#------------------------------------------------------------------------------
#
#     History
#       16.01.2009 S. Lipp (Variables changed and edited decently)
#       06.03.2013 S. Benzinger (now works for Windows and Linux)
#       03.04.2014 S. Benzinger (small changes))
#		12.05.2015 A. Koksharov, usage with autoconf (multi-platform deployment)
###############################################################################


#------------------------------------------------------------------------------
# Parameters from the configuration script
#------------------------------------------------------------------------------
CC = gcc
FC = gfortran
USE_MINI = 0
prefix = /usr/local
CFLAGS = -g -O2
FCFLAGS = -g -O2 -cpp -DVERSION_NUM='"unknown"' -DVERSION_DATE='"unknown"' -DCOMLIB_NUM='"unknown"' -DCOMLIB_DATE='"unknown"'
LIBS =  

#------------------------------------------------------------------------------
# directories
#------------------------------------------------------------------------------
HOMEDIR  = .
# Sources
INPSRC   = $(HOMEDIR)/hominp
RUNSRC   = $(HOMEDIR)/homrun
# directories for library
LIBDIR   = $(HOMEDIR)/library
# Object linking rules
MAKEDIR  = $(HOMEDIR)/mkincludes

# Output
BINDIR = $(HOMEDIR)/bin
OBJDIR = $(HOMEDIR)/obj

#------------------------------------------------------------------------------
# assign VPATH to search for sources 
#------------------------------------------------------------------------------
VPATH = $(INPSRC):$(RUNSRC):$(LIBDIR)
# Force .mod file to the object dir for gfortran
ifeq ($(FC),gfortran)
  FCFLAGS += -J$(OBJDIR)
endif
ifeq ($(FC),ifort)
  FCFLAGS += -module $(OBJDIR)
endif
#------------------------------------------------------------------------------
# rule for object files
#------------------------------------------------------------------------------
$(OBJDIR)/%.o: %.f
	$(FC) $(FCFLAGS) -c $< -o $@
$(OBJDIR)/%.o: %.f90
	$(FC) $(FCFLAGS) -c $< -o $@
$(OBJDIR)/%.o: %.F
	$(FC) $(FCFLAGS) -c $< -o $@
$(OBJDIR)/%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@
#------------------------------------------------------------------------------
# hominp objects
#------------------------------------------------------------------------------
include $(MAKEDIR)/hominp_objects.mk
INPOBJ := $(addprefix $(OBJDIR)/,$(HOMIOBS))
#------------------------------------------------------------------------------
# homrun objects
#------------------------------------------------------------------------------
include $(MAKEDIR)/homrun_objects.mk
include $(MAKEDIR)/homrun_library_objects.mk

HOMROBS=$(HOMRLO) $(HOMRIO)
RUNOBJ := $(addprefix $(OBJDIR)/,$(HOMROBS))

.PHONY : inp run clean help cleanobj cleanbin cleanmod install
###############################################################################
#    Linking objects to binaries
###############################################################################
all : inp run
inp : $(INPOBJ)
	$(FC) $(INPOBJ) $(LDFLAGS) -o $(BINDIR)/hominp
run : $(RUNOBJ)
	$(FC) $(RUNOBJ) $(LDFLAGS) $(LIBS) -o $(BINDIR)/homrun
###############################################################################
#    Cleaning objects
###############################################################################
clean     : cleanbin cleanobj cleanmod
cleanbin  :
	rm -f $(BINDIR)/*$(EXEEXT)
cleanobj  :
	rm -f $(OBJDIR)/*.o
cleanmod  :
	rm -f $(OBJDIR)/*.mod
	rm -f *.mod

###############################################################################
#    Help for the Makefile
###############################################################################
help: 
	@echo
	@echo "use ./configure to set up the make file (i.e. compiler and options)"
	@echo " "
	@echo "Options: "
	@echo "make all  	- compile hominp and homrun"
	@echo "make clean 	- remove objects and binaries"
	@echo "make inp 	- compile hominp only"
	@echo "make run		- compile homrun only"
	@echo "make help	- show this help"
	
