!****************************************************************************************!
!   Module for homrea parameters
!
!   Parameter glossary:
!       NEQ
!       NSPEC
!       IEQ
!   Best practice for using the parameters:
!
!
!
module par
    ! Module for storing general parameters
    use homerr
    implicit none

    public 

    integer, parameter:: Lsymb=16, Mnspe=1086, Mnele=10, Mnrea=10000, Mnthb=90,&
        Mnxequ=5, Mnequ = (Mnspe+Mnxequ), Mnsen=8500, Mnpoi=1000, Mncre=20,&
        Mnrrea=300000, Mnirea=200000, Mnopt=60, Mnrspe = (43+4+Mnspe)*Mnspe, Mnvert = 5000
    ! Block for Rpar and Ipar arrays:
    integer, parameter:: Mnrpar=120001, Mnipar=14001

    !Block for work arrays:
    integer, parameter:: Lrwork=80200600, Liwork=8791750
    
    ! Double precision definition
    integer, parameter:: dp = kind(0.d0)

    integer, protected, save:: Neq, Ieq, Ncon, U_out
    real(dp):: Rgas= 8.3141, PATM = 101235, One=1.d0, Zero=0.d0, Eps=1.0d-21 

    ! Options from the input file
    integer, protected, save:: Nopt(Mnopt)
    ! Integer and real parameter array
    integer:: Ipar(Mnipar)
    real(dp):: Rpar(Mnrpar)

    ! Species symbols
    character(len=Lsymb):: Symb(Mnspe+Mnthb+1+2), Esymb(Mnele)

    ! 
    logical, save:: Tso, Pcon, Vcon, Tcon, Detona, Test
    integer, save:: Multiz, Locana, Globana, Ncomplx, Nengin, Nexcet, Npsr, Ntrans, Nwall

    ! Note, the following parameters have been in common blocks before

    ! Block for species data
    integer, save:: Nspec, Nele, Nrspe, Neps, Nsig, Nhl, Nhsw, Neta, Nlcoef, Nlcoem, &
        Nlcoed, Ndcoef, Lrspec
    real(dp), public:: Rspec(Mnrspe)
    real(dp), allocatable, save:: Xmol(:), Xminv(:), Emol(:), &
        Xe(:), Sspv(:), Hlhh(:,:), Hinf(:,:)

    ! Block for reaction dimensions
    integer, save:: Nreac, Nm, Nrins, Nrtab, Ninert

    ! Rreac and parameters
    integer, save:: Lrreac, Npk, Ntexp, Nexa, Nvt4, Nvt5, Nvt6, Nainf, Nbinf, &
        Neinf, Nt3, Nt1, Nt2, Nafa, Nbfa, Nxzsto, Nratco
    real(dp), save:: Rreac(Mnrrea) 

    ! Ireac and parameters
    integer, save:: Lireac, Nirev, Niarrh, Nmatll, Nmatlr, Nnrvec
    integer, save:: Ireac(Mnirea)

    ! Block for complex reactions
    integer, save:: Nbrut 
    integer, allocatable, save:: nums(:, :)
    real(dp), allocatable, save:: Stobr(:, :), Ord(:, :), Abru(:), Ebru(:), Tbru(:)
    
    ! Block for output control
    integer, save :: Konint, Kontab, Konplo, Konsen, Konmec, Konana, Konred, Nsc, Icrit, &
        Kogsen, Konrst
    integer, allocatable, save:: Konspe(:), Konsid(:)
    logical, save:: Molefr, Massfr, Lplo

    ! Block for integration
    real(dp):: Rtol(1), Atol(1), Rtols(1), Atols(1), H, Hmax, Tbeg, Tsteps, Tend, Step
    real(dp):: Profile(3, Mnpoi), Profile_time(3, Mnpoi)
    ! Initial profile
    real(dp), allocatable:: Inipro(:), Smf0(:)
    real(dp):: p0, T0, V0

    ! Block for sensitivity analysis
    integer:: Lsmax, Nsens, Nrsens, Nisens, Npsens, Irsens, Iisens, Ipsens, Isensi(Mnsen)

    ! Block for LOCS0D
    integer:: Nw, Nws, Np, Nk, Ni, Nh
    ! External functions
    real(dp):: ddot
    external ddot
    contains
        subroutine init_par(errg)
            ! Subroutine to initialize all parameters
            use, intrinsic:: iso_fortran_env, only: stdout=>output_unit
            
            integer, intent(out):: errg

            ! Local parameters
            integer:: nfil3, errl, nfil10

            character(len= 8) :: date_str
            character(len=10) :: time_str
            logical:: verb, stp

            ! Set global error indicator to 0

            ! TODO: Implement command line argument parsing
            ! For now: Always verbose
            verb = .true.
            if (verb) then
                U_out = stdout
            else
                call date_and_time(DATE=date_str, TIME=time_str)
                open(newunit=U_out, file='log_'//date_str//time_str, status='new')
            end if 

            ! Open fort.3 file for reading
            write(U_out, *) 'Reading fort.3...'
            open(newunit=nfil3, file='fort.3', status='old', iostat=errl)
            if (errl /= 0) call throw_error(U_out, 'Could not open file fort.3') 
            ! TODO nfil10, stp (former stop) is unused
            call read_fort3(nfil3, errl)
            
            return
        end subroutine

        subroutine read_fort3(nfil3, errg)
            integer, intent(in):: nfil3
            integer, intent(out):: errg
            

            ! Local parameters
            integer:: i, k, errl, idum, konise, konrse, npo(3)
            real(dp):: cin, csum, cn, hl, pl, work1(20*Mnspe)
            integer, allocatable:: ifsp(:)
            real(dp), allocatable:: xini(:)

            real(dp):: rho
            real(dp), dimension(1):: dxds, dtds, dpds 

            rewind nfil3
           
            ! Input of options
            read(nfil3,800,iostat=errl) (Nopt(i), i=1, Mnopt)
            if (errl /= 0) call throw_error(U_out, ' While reading fort.3')
            
            Test = (nopt(39) > 4)
            
            Tso = (nopt(40) == 1) 
            if (test) write(U_out,800) (nopt(i), i=1, Mnopt)

            Multiz = Nopt(32)

            if (nopt(5) /= 0) then
                Nengin = nopt(5)
                Multiz = max(1,Nopt(32))
            end if

            Nexcet = Nopt(36)
            ! If center option is chosen, VPRO option has to be 1
            if (Nexcet > 0) Nopt(1) = 1

            Ntrans = Nopt(21)
            if (Ntrans == 1) Ntrans = 0

            Vcon = (Nopt(1) /= 0)
            Pcon = .not.Vcon
            Tcon = (Nopt(2) /= 0)
            Npsr = Nopt(6)

            Nwall = 0
            if (Nopt(4) > 0) then
                Nwall = Nopt(4)
                Multiz = max(1, Nopt(32))
            end if

            Ncomplx = Nopt(35)
            
            Detona = (Nopt(2) == 2) 

            if (Pcon .and. .not.tcon) Ieq = 1
            if (Vcon .and. .not.tcon) Ieq = 2
            if (Pcon .and. Tcon) Ieq = 5
            if (Vcon .and. Tcon) Ieq = 6
            
            Ipar(10) = Ieq

            Locana = Nopt(41)
            Globana = Nopt(42)

            if (Npsr /= 0 .and. .not.Vcon) call throw_error(U_out, 'If using option&
                PSR, Vprofile has to be defined')

            ! Input of number of species and elements
            read(nfil3, 801) Nspec
            if (errl /= 0) call throw_error(U_out, 'While reading number of species &
                in fort.3')
            if (test) write(U_out, 801) Nspec
            if (Nspec > Mnspe) call throw_error(U_out, 'Number of species greater &
                than the maximum allowed. Please change it in par.f90')


            read(nfil3, 801, iostat=errl) Nele
            if (errl /= 0) call throw_error(U_out, 'While reading number of elements &
                in fort.3')
            if (test) write(U_out, 801) Nele
            if (neq > Mnele) call throw_error(U_out, 'Number of elements greater &
                than the maximum allowed. Please change it in par.f90') 

            ! Allocate some arrays of which we know the size
            Nrspe = (34+4*Nspec)*Nspec
            allocate(Xmol(Nspec), Xminv(Nspec), Emol(Nele), Xe(Nspec*Nele), &
                Hlhh(7, 2*Nspec), Hinf(3, Nspec)) 
            
            ! Input of species symbols and masses

            errl = 1

            ! This is an ildm option, which will be ommited for now
            !Icaltr = Nopt(21)
            !if (Icaltr > 0) errl = 0

            call inpspe(Nspec, Nele, nfil3, U_out, 1, errl, Symb, Xmol, esymb, &
                Emol, Xe, Nrspe, Rspec, Neps, Nsig, Nhl, Nhsw, Neta, Nlcoef, Nlcoem, &
                Nlcoed, Ndcoef)
            if (errl > 0) call throw_error(U_out, 'In subrotuine inpspe.')
            Ipar(2) = Nspec + 2

            ! Calcualte inverse molar mass
            if (Nopt(7) /= 0) then
                Xminv = One/Xmol
            end if

            ! Input of thermodynamic data
            do i=1, Nspec
                read(nfil3,807, iostat=errl) (Hlhh(k, i), k=1, 7), (Hlhh(k, i+Nspec),&
                    k=1, 7), (Hinf(k, i), k=1, 3)
                if (errl /= 0) call throw_error(U_out, 'While reading thermodynamic &
                    data in fort.3')
                if (test) write(U_out, 804) Symb(i), (Hlhh(k, i), k=1, 7),&
                    Symb(i), (Hlhh(k, i+Nspec), k=1,7), Symb(i), (Hinf(k, i), k=1, 3)
            end do

            ! Input of elementary reaction mechanism

            NRtab = 20
            call inpmec(Nspec, Symb, nfil3, 10, U_out, 0, errl, Nreac, Nm, Nrins, nrtab,&
                Ninert, &
                Mnirea, Ireac, Nirev, Niarrh, Nmatll, Nmatlr, Nnrvec,&
                Mnrrea, Rreac, Npk, Ntexp, Nexa, Nvt4, Nvt5, Nvt6, Nxzsto, Nratco,&
                1, idum, Nainf, Nbinf, Neinf, Nt3, Nt1, Nt2, Nafa, Nbfa)
            if (Nm > Mnthb) call throw_error(U_out, 'Number of third bodies greater&
                than maximum allowed. Please change in in par.f90.')

            ! Not Implemented option ILDM
            if (nopt(7) /= 0) call throw_warning(U_out, 'ILDM not implemented')

            ! Input of complex reaction mechanism
            read(nfil3, 801, iostat=errl) Nbrut
            if (errl /= 0) call throw_error(U_out, 'While reading number of complex &
                reactions in fort.3')
            if (Test) write(U_out, 801) Nbrut

            if (nbrut > 0) then
                do i=1, Nbrut
                    ! Allocate arrays for complex reactions
                    allocate(Stobr(Nbrut, 6), Nums(Nbrut, 6), Ord(Nbrut, 6),&
                        Abru(Nbrut), Ebru(Nbrut), Tbru(Nbrut))
                    read(nfil3, 810, iostat=errl) (Stobr(i,k), k=1, 6)
                    if (errl /= 0) call throw_error(U_out, 'While reading complex &
                        reactions in fort.3')
                    if (Test) write(U_out, 810, iostat=errl) (Stobr(i, k), k=1, 6)
                    if (errl /= 0) call throw_error(U_out, 'While reading complex &
                        reactions in fort.3')

                    read(nfil3, 811, iostat=errl) (Nums(i, k), k=1, 6)
                    if (errl /= 0) call throw_error(U_out, 'While reading complex &
                        reactions in fort.3')
                    if (Test) write(U_out, 811) (Nums(i, k),k =1, 6)

                    read(nfil3, 810, iostat=errl) (Ord(i, k), k=1, 6)
                    if (errl /= 0) call throw_error(U_out, 'While reading complex &
                        reactions in fort.3')
                    if (Test) write(U_out, 810) (Ord(i, k), k=1, 6)

                    read(nfil3, 802, iostat=errl) Abru(i), Ebru(i), Tbru(i)
                    if (errl /= 0) call throw_error(U_out, 'While reading complex &
                        reactions in fort.3')
                    if (Test) write(U_out, 802, iostat=errl) Abru(i), Ebru(i), Tbru(i)
                    if (errl /= 0) call throw_error(U_out, 'While reading complex &
                        reactions in fort.3')
                end do
            end if
            
            ! Read information about output control
            allocate(Konspe(Nspec), ifsp(Nspec))
            read(nfil3, 809) (ifsp(i), i=1, Nspec)

            ! Compute icospe
            nsc = 0
            do i=1, Nspec
                if(Ifsp(i) /= 0) then
                    Nsc = nsc + 1
                    Konspe(nsc) = i
                end if
            end do

            allocate(Sspv(20*Nspec))
            ! Set up thermodynamics vector
            call dcopy(Nspec, Xmol, 1, Sspv(1), 1)
            call dcopy(14*Nspec, Hlhh, 1, Sspv(Nspec+1), 1)
            call dcopy(3*Nspec, Hinf, 1, Sspv(15*Nspec+1), 1)

            ! Set up locations
            if (Tcon) then
                Neq = Nspec 
            else if (Detona) then
                Neq = NSpec + 4
            else
                Neq = Nspec + 2 
            end if

            ipar(1)=Neq
            ipar(3)=Nspec
            ipar(6)=Nele
            ipar(9)=Nreac
            ipar(7)=Nele+2

            ! Set up pointers for output control
            if (.not.Tcon)  then
                Nsc=Nsc+1
                Konspe(Nsc)=Nspec+1
            end if

            ! Set uo pointers for sensitivity analysis
            konise = nopt(9)
            Konsen = nopt(11)
            Kogsen = nopt(10)

            if (Konsen /= 0 .and. Kogsen /= 0) call throw_error(U_out,'Error - Canot do both &
                local and global analysis. Stopping homrun.')

            if (konsen > 0 .and. Nsc == 0) Konsen = 0
            if (kogsen > 0 .and. Nsc == 0) Kogsen = 0
            konrse = 0
            if (Konsen > 0) konrse = 1
            if (Kogsen > 0) konrse = 2
            
            call inisen(Konrst, konise, Ireac(Nirev))
            
            Konrst=0

            ! Output in mole fractions
            Molefr=(nopt(17) == 1)

            ! Output in mass fractions
            Massfr=(nopt(17) == 2)

            ! Logarithmic plot
            Lplo=(nopt(19)==1)

            ! Output of intermediate results
            Konint=Nopt(39)
            ! Output of tables at the end of integration
            Kontab=Nopt(15)
            ! Output of plots
            Konplo=Nopt(16)
            ! Output of data for mechanism analysis
            Konmec=Nopt(21)
            ! Eigenvector analysis
            Konana=Nopt(7)

            ! ILDM not implemted
            if (Nopt(13) /= 0) call throw_warning(U_out, 'ILDM not implemented. &
                Igoring option')

            ! Redim also not implemented
            if (any([Nopt(22),Nopt(33)] /= 0)) call throw_warning(U_out, 'REDIM not &
                implemented. Ignoring option')

            ! Assign symbols for physical variables
            ! TODO check if they will be overwritten by third body symbols?
            Symb(Nspec+1)=' T (K)  '//repeat(' ', Lsymb-8)

            ! Write output data for mechanism analysis
            if (Konmec /= 0) call output_mech_ana(errl)

            ! Solution method - only limex is implemented at the moment
            if (Nopt(20) /= 1) call throw_warning(U_out, 'WARNING: At the moment only &
                the limex solver is implemented')

            ! Intgration control
            read(nfil3,*) npo(1), npo(2), npo(3)


            do k=1, 3
                if (npo(k) > Mnpoi) call throw_error(U_out, 'Too many points in profile.')
                do i=1, npo(k)
                    read(nfil3,813) Profile(k,i), Profile_time(k,i)
                end do
            end do
           
            allocate(xini(Nspec), inipro(Nspec))
            ! Read initial concentrations
            read(nfil3,814) xini
            if (test) write(U_out,814) xini

            ! Read integration controls
            read(nfil3,815) Tbeg, Tend, Tsteps
            if (test) write(U_out,*) Tbeg, Tend, Tsteps

            read(nfil3,815) Atol, Rtol, Step, Atols, Rtols
            if (test) write(U_out,*) Atol, Rtol, Step, Atols, Rtols 


            P0 = Profile(1,1)
            V0 = Profile(2,1)
            T0 = Profile(3,1)

            Nw = 1
            Nws = 0
            Np = Nspec + 1
            Nk = Nspec + 2
            Ni = Nspec + 3
            Nh = Nspec + 4

            cin = P0/(Rgas*T0)
            csum = sum(xini)

            errl = 0
            allocate(Smf0(Neq))
            Inipro = ([(xini(i)*cin/csum, i=1,Nspec)])
            work1(1:20*Nspec) = Sspv(1:20*Nspec)
            call trassp(-Ieq, Neq, Smf0, Nspec, T0, cn, Inipro, rho, hl, pl, work1, errl, &
                0, dxds, 1, dtds, dpds)
            if (errl > 0) call throw_error(U_out, 'In trassp. Couldnt convert initial profile...')
            ! Regular exit
            return
            ! Format statements 
        800 format(12x,20i3)
        801 format(i5)
        802 format(12x, 5(1pe11.4,1x))
        804 format(' HL  (',a,')',4(1x,1pe11.4),/,11x,3(1x,1pe11.4),/,&
                   ' HH (',a,')',4(1x,1pe11.4),/,11x,3(1x,1pe11.4),/,&
                   ' HINF(',a,')',4(1x,1pe11.4))
        807 format(16x,5(1pe11.4,1X),/5(1pe11.4,1x),/,&
                4(1pe11.4,1x),2x,3(1x,1PE9.2))
        809 format(1(15X,20i3))
        810 format(12x, 8(f6.3,1x))
        811 format(12x, 8(2x,i2,3x))
        812 format(1x,i4,1x,i4,i4) 
        813 format(12x,1pe9.2,13x,1pe9.2)
        814 format(12x,5(1pe20.13,1x))
        815 format(12x,5(1pe11.4,1x))
            ! Error exists
            return
        end subroutine

        subroutine output_mech_ana(errg)
            !TODO implement mechanism
            integer, intent(out):: errg

            errg = 0
            return
        end subroutine


        subroutine inisen(konrse, konise, irev)
            integer, intent(in):: konrse, konise
            integer, intent(inout):: irev(*)
            integer:: i, iii
            Iisens = 1
            Nisens = 0
            select case (konise)
                case (1)
                    Nisens = Neq
                case (2)
                    Nisens = Nsc
            end select

            do i=1, Nisens
                Isensi(Iisens-1+i) = i
            end do
            Irsens = Iisens + Nisens
            Nrsens = 0
            do i =1, Nreac
                iii = 0
                select case(konrse)
                    case (1)
                        iii = Irev(i)
                    case (2)
                        iii = -1
                end select
                Irev(i) = iabs(Irev(i)) - 1
               
                if (iii < 0) then
                    Nrsens = Nrsens + 1
                    Isensi(Irsens-1+Nrsens) = i
                end if
            end do

            Npsens = 0
            Ipsens = Irsens + Nrsens
            Nsens = Nrsens + Nisens + Npsens
            return
                    
        end subroutine
end module par
