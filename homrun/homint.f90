module homint
    use homerr
    use par
    use homtra
    implicit none

    contains
        subroutine integrate(trajectory, errg)
            type(tra), intent(inout):: trajectory
            integer, intent(inout):: errg


            ! Local variables
            integer:: ir, irmax, nzc, nzv, icall, errl, iw(Liwork), ijob(20) 
            real(dp):: time, hmax, h, rw(Lrwork) 
            real(dp), allocatable:: smf(:), smfp(:)
            
            ! Set integration parameters
            hmax = 1.d10 
            h = step

            nzv = 0 
            nzc = trajectory%ndim

            ijob = 0 
            ijob(1) = 1
           
            ! Check if integrration time ok Check if trajectory contains at least one point
            if (trajectory%nvert < 1) call throw_error(U_out, 'Cannot integrate trajectory & 
                without an initial point.')

            allocate(smf(trajectory%ndim), smfp(trajectory%ndim))

            
            time = trajectory%time(trajectory%nvert) 
            ir = trajectory%nvert
            smf = trajectory%dat(1:trajectory%ndim, trajectory%nvert)

            icall = 1 
            irmax = trajectory%nm_nvert 

            do while((time < tend) .and. (ir < irmax)) 
                call xlimex(trajectory%ndim, nzc, nzv, homlef, homrig, dumjac, time, smf, smfp, &
                    tend, Rtol, Atol, Rtols(1), Atols(1), hmax, h, ijob, Lrwork, rw, liwork,iw, &
                    icall, Rpar, Ipar, U_out) 
                if (ijob(7) < 0) then 
                    errg = 1 
                    exit 
                end if

                ir = ir+1 
                if (ir > trajectory%nm_nvert) then 
                    call throw_warning(U_out, ' Maximum number of points reached during &
                        integration. Stopping here, we might not be in equilubrium though') 
                    exit 
                end if 
                icall = 2 
                trajectory%nvert = ir
                trajectory%dat(ir,:) = smf 
                icall = 2 
                ijob(7) = 0 
            end do

        end subroutine

        subroutine homres(job, time, smf, rate, errg, jac, ssp, g)
            integer, intent(in):: job
            integer, intent(out):: errg
            real(dp), intent(inout):: time
            real(dp), intent(inout):: smf(Neq)
            real(dp), optional, intent(out):: rate(Neq), jac(Neq, Neq), ssp(5), g(Nreac) 


            integer:: i, iex, lrw1, iprim, errl
            real(dp):: tl, cl, cp, vl, rho, hspec, pl, s, mms, stomcom, ss, dxds(1), dtds(1), dpds(1),&
                xmolm, fac, cil(Mnspe+Mnthb+1), work1(Mnrea), work2(Mnspe)
            real(dp), dimension(Nspec):: dodt, si, hi, dsdpsi, fn, yn, help1, help2, rat
            real(dp), dimension(Nreac):: dgdt, gr
            real(dp):: dgdc(Nreac, Nspec), dodc(Nspec, Nspec)
            logical:: psr

            !call trassp(-3, Neq, smf, Nspec, tl, cl, cil, rho, hspec, pl, Sspv, iex, &
            !    0, dxds, 1, dtds, dpds) 
            !xmolm = rho/cl

            lrw1 = Mnrea 
            errl = 0 
            iprim = 0

            psr = (Npsr > 0)

            i = Ieq
          
            ! Workaround for the trassp method... TODO modify it so it can have a different work array
            work2(1:) = Sspv(1:)

            call trassp(i, Neq, smf, Nspec, tl, cl, cil, rho, hspec, pl, work2, errl, 0, dxds, 1, dtds, dpds)
            if (errl > 0) call throw_error(U_out, 'In trassp...')

            if (Tcon) call lin_interp(Profile(3,:), Profile_time(3,:), time, tl, ss)
            if (Pcon) call lin_interp(Profile(1,:), Profile_time(1,:), time, pl, ss)
            if (Vcon .or. psr) call lin_interp(Profile(2,:), Profile_time(2,:), time, vl, ss)

            tl = max(1.d1, tl)
            iex = iex
            call mechja(iex, Nspec, Nreac, Nm, Nrins, Nrtab, Ireac(Nmatll), & 
                Ireac(Nnrvec), Rreac(Npk), Rreac(Ntexp), Rreac(Nexa), Rreac(Nvt4), &
                Rreac(Nvt5), Rreac(Nvt6), Ireac(Niarrh), Rreac(Nxzsto), Rreac(Nratco), & 
                cl, cil, tl, rat, Nrsens, Isensi(Irsens), Rpar(Irsens), lrw1, work1, iprim, U_out, &
                errl, Rpar(Nainf), Rreac(Nbinf), &
                Rreac(Neinf), Rreac(Nt3), Rreac(Nt1), Rreac(Nt2), Rreac(Nafa), &
                Rreac(Nbfa), Hlhh, Hinf, 1, 1, dgdc, dgdt, dodc, dodt)
            ! For now; TODO adjust so it can handle different kinds of parameters
            rate = Zero
            rate(3:2+Nspec) = rat(1:Nspec)/rho 
            if (job == 2 .or. job == 3) then 
                dodt = dodt + rat/tl 
                fac = -1/rho

                call dger(Nspec, Nspec, One, help1, 1, help2, 1, jac(3,3), Neq)

                hi = Sspv(18*Nspec+1:19*Nspec) 
                cp = dot_product(Sspv(19*Nspec+1:20*Nspec), smf(3:Neq)) 
                jac(1:2, 1:Neq) = Zero
                jac(3:Neq, 3:Neq) = dodc

                help1(1:Nspec) = matmul(dodc, smf(3:Neq)) 
                help2(1:Nspec) = hi/(tl*cp) - xmolm

                call dger(Nspec, Nspec, One, help1, 1, help2, 1, jac(3,3), Neq)

                fac = -One/(rho*cp) 
                jac(3:Neq, 3:neq) = jac(3:Neq, 3:Neq) + fac*matmul(reshape(dodt, & 
                    [Nspec, 1]), reshape(hi, [1, Nspec]))
                
                jac(3:Neq, 1) = - matmul(dodc, smf(3:neq))/(tl*cp) 
                jac(3:Neq, 1) = jac(3:Neq, 1) + dodt/(rho*cp) 
                jac(3:Neq, 2) = help1/pl 
            else if (job == 3) then 
                g(1:Nreac) = work1(1:Nreac)/rho

                ss = Zero 
                mms = sum(smf(3:Neq))
                do i=1, Nspec 
                    call calhsi(Nspec, i, tl, Hlhh, Hinf, hi(i), si(i), U_out) 
                    if (smf(i+2) > Eps) then 
                        si(i) = si(i) - Rgas*log(smf(i+2)*mms) 
                        dsdpsi(i) = si(i) - Rgas 
                        ss = ss + si(i)*smf(i+2) 
                    end if 
                end do 
                ssp(1) = ss
                ssp(2) = tl 
                ssp(3) = rho 
                ssp(4) = mms 
            end if 
            return

        end subroutine homres

        subroutine homrig(nneq, nzv, time, s, f, bv, ir, ic, rpar, ipar,  ifc, ifp)
            integer:: nneq, nzv, ifc, ir(*), ic(*), ifp(*), ipar(*)
            real(dp):: time, s(*), f(*), bv(*), rpar(*)
            
            !Local vvaribvles
            integer:: errl

            call homres(0, time, s, f, errl)
            return
        end subroutine

        subroutine homlef(nzc, b, ir, ic)
            integer:: i, nzc, ir(*), ic(*)
            real(dp):: b(*)

            do i = 1, nzc
                ir(i) = i
                ic(i) = i
            end do
            B(1:nzc) = One

            return
        end subroutine

        subroutine dumjac
            stop
        end subroutine dumjac

        subroutine lin_interp(yarray, xarray, xval, yval, dydx) 
            real(dp), intent(in):: xval, yarray(*), xarray(*) 
            real(dp), intent(out) :: yval, dydx

            integer:: i

            i = 2
            do while (xval < xarray(i))
                dydx=(yarray(i)-yarray(i-1))/(xarray(i)-xarray(i-1))
                yval=yarray(i-1)+dydx*(xval-xarray(i-1))
            end do

            return
        end subroutine


end module
