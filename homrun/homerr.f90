module homerr
    implicit none
    ! Module for error messages and warings

    contains
        subroutine throw_error(errfil, msg)
            integer, intent(in):: errfil
            character(len=*), intent(in):: msg

            write(errfil, *) achar(27)//'[31m FATAL ERROR:'//achar(27)//'[0m'//&
                trim(msg)
            write(errfil, *) achar(27)//'[31m HOMRUN2: IRREGULAR EXIT'//achar(27)//'[0m'
            stop
        end subroutine

        subroutine throw_warning(errfil, msg)
            integer, intent(in):: errfil
            character(len=*), intent(in):: msg

            write(errfil,*) achar(27)//'[37m WARNING:'//achar(27)//'[0m'//&
                trim(msg)
        
            return
        end subroutine

end module homerr
