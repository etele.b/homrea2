program homrea2
    use par
    implicit none

    ! Local variables
    integer:: errl
   
    ! Print something nice
    call print_welcome_banner()
    ! Get input data
    call init_par(errl)
    
    ! Check if something has to be done bcause of the parameters

    ! Do detailed integration
    write(U_out,*) repeat('#',100)
    write(U_out,*) '#   Starting detailed integration..'
    write(U_out,*) repeat('#',100)

    call run_homint()

    call print_congratulations_banner()

    stop
end program

! Execution routines for the different modules

! Homint (simple, homogenous integration module)

subroutine run_homint()
    use par
    use homint

    type(tra):: trajectory

    integer:: errl
    ! Initialize trajectory and set initial point
    call trajectory%init(Neq, Mnvert)

    call trajectory%add_inip()

    ! Check time control

    ! Run integration
    call integrate(trajectory, errl)



end subroutine


subroutine print_welcome_banner()
    implicit none

    print *, achar(27)//'[34m'//repeat('#',100)
    print *, '#'//achar(27)//'[36m RUNNING...'//achar(27)//'[34m'//repeat(' ',87)//'#'
    print *, '#'//repeat(' ',28)//'__  ______  __  _______  __  ___   _____ '//&
        repeat(' ', 29)//'#'
    print *, '#                           / / / / __ \/  |/  / __ \/ / / / | / /__ \'//&
        repeat(' ', 29)//'#'
    print *, '#                          / /_/ / / / / /|_/ / /_/ / / / /  |/ /__/ /'//&
        repeat(' ', 29)//'#'
    print *, '#                         / __  / /_/ / /  / / _, _/ /_/ / /|  // __/ '//&
        repeat(' ', 29)//'#'
    print *, '#                        /_/ /_/\____/_/  /_/_/ |_|\____/_/ |_//____/ '//&
        repeat(' ', 29)//'#'
    print *, '# Version:'//repeat(' ',89)//'#'
    print *, repeat('#',100)//achar(27)//'[0m'
          
    return
     
end subroutine 

subroutine print_congratulations_banner()
    implicit none

    print *, achar(27)//'[34m'//repeat('#',100)
    print *, "#"//repeat(' ',10), "    ______                             __        __      __  _"&
        //repeat(' ',26)//"#"
    print *, "#"//repeat(' ',10), "   / ____/___  ____  ____ __________ _/ /___  __/ /___ _/ /_(_&
        )___  ____  _____"//repeat(' ',9)//"#"
    print *, "#"//repeat(' ',10), "  / /   / __ \/ __ \/ __ `/ ___/ __ `/ __/ / / / / __ `/ __&
        / / __ \/ __ \/ ___/"//repeat(' ',9)//"#"
    print *, "#"//repeat(' ',10), " / /___/ /_/ / / / / /_/ / /  / /_/ / /_/ /_/ / / /_/ / /_&
        / / /_/ / / / (__  ) "//repeat(' ',9)//"#"
    print *, "#"//repeat(' ',10), " \____/\____/_/ /_/\__, /_/   \__,_/\__/\__,_/_/\__,_/\__/_/&
        \____/_/ /_/____/  "//repeat(' ',9)//"#"
    print *, "#"//repeat(' ',10), "                  /____/"//repeat(' ',64)//"#"
    print *, '#'//achar(27)//'[36m SIMULATION WAS SUCCESSFUL!'//achar(27)//'[34m'//repeat(' ',71)//'#'
    print *, repeat("#", 100)//achar(27)//'[0m'
end subroutine
