module homtra
    use par
    use homerr
    implicit none

    ! Trajectory type
    type:: tra
        integer:: nvert, ndim, nm_nvert
        real(dp), allocatable:: time(:), dat(:,:), yeq(:), yi(:)

        ! Trajectory properties
        contains
            procedure:: init    => init_tra
            procedure:: delete  => del_tra
            procedure:: prop    => prop_tra
            procedure:: append  => append_tra
            procedure:: add_inip=> add_initial_point

            procedure:: compare         => comp_tra
            procedure:: ignition_delay  => igndel
            procedure:: half_time       => halftim
            procedure:: int_spec        => intsp
            procedure:: minmax          => minmax

    end type tra

    contains

        subroutine init_tra(this, ndim, nm_vert)
            class(tra), intent(inout):: this
            integer, intent(in):: ndim, nm_vert


            allocate(this%time(nm_vert), this%dat(ndim, nm_vert), this%yi(ndim),&
                this%yeq(ndim))
            this%nm_nvert = nm_vert
            this%ndim = ndim

            this%nvert = 0

            return
        end subroutine

        subroutine del_tra(this)
            class(tra), intent(inout):: this 

            deallocate(this%time, this%dat, this%yi, this%yeq)
            return
        end subroutine

        subroutine append_tra(this, other)
            class(tra), intent(inout):: this
            class(tra), intent(in):: other

            ! Check if first trajectory initialized, if not, throw Error
            if (.not.allocated(this%time) .or. .not.allocated(this%dat))&
                call throw_error(U_out, 'Cannot append to non-initialized &
                trajectory.')

            ! Check if seconf trajectory not initialized
            if (.not.allocated(other%time) .or. .not.allocated(other%dat)) then
                call throw_warning(U_out, 'Appending empty trajectory, orginal &
                    trajectory will not be changed')
                return
            end if

            if (this%nvert == 0 .and. other%nvert == 0) then
                ! Both trajectories empty, return empty trajectory
                return
            end if

            ! Overflow
            if (this%nvert+other%nvert > this%nm_nvert) then
                call throw_warning(U_out, 'Maximum number of points reached while &
                    appending trajectory. Overflow points will not be added.')
                this%dat(this%nvert+1:this%nm_nvert,:) = &
                    other%dat(1:this%nm_nvert-other%nvert-1,:)
                this%time(this%nvert+1:this%nm_nvert) = &
                    other%time(1:this%nm_nvert-other%nvert-1)
            else
                this%dat(this%nvert+1:this%nvert+other%nvert,:) = &
                    other%dat(1:other%nvert,:)
                this%time(this%nvert+1:this%nvert+other%nvert) = &
                    other%time(1:other%nvert)

            end if
            return
        end subroutine 


        subroutine prop_tra(this)
            class(tra), intent(inout):: this

        end subroutine

        function comp_tra(this, other) result(rdif)
            class(tra), intent(in):: this, other
            real(dp):: rdif

            rdif = 0
            return
        end function

        subroutine halftim(this,species_number, ht, pos, smf)
            class(tra), intent(in):: this
            integer, intent(in):: species_number
            integer, intent(out):: pos
            real(dp), intent(out):: ht
            real(dp), allocatable:: smf(:)

            integer:: i
            real(dp):: halft, ref, fd

            ref= (this%dat(species_number+2, 1)+&
                this%dat(species_number+2, this%nvert))/2
            do i=2, this%nvert -1
                if (this%dat(species_number+2, i-1) <= ref) then
                    fd= min(-Eps, this%dat(species_number+2, i) - &
                        this%dat(species_number+2, i-1))
                    ht= this%time(i-1) + (this%time(i) - this%time(i-1))&
                        /fd*(ref - this%dat(species_number+2, i-1))
                    pos = i
                    smf = this%dat(1:this%ndim,i)
                    return
                end if
            end do

        end subroutine

        subroutine igndel(this, species_number, ig, pos, smf)
            class(tra), intent(in):: this
            integer, intent(in):: species_number
            integer, intent(out):: pos
            real(dp), intent(out):: ig
            real(dp), allocatable, intent(out):: smf(:)

            integer:: i
            real(dp):: ss, slope
            ig = Zero
            ss = Zero

            do i=1, this%nvert-1
                slope = (this%dat(species_number+2, i+1) - &
                    this%dat(species_number+2, i)/max(Eps, this%time(i+1) - &
                    this%time(i)))
                if (slope > ss) then
                    pos = i
                    ss = slope
                    ig = this%time(i)
                    smf = this%dat(1:this%ndim, i)
                end if
            end do
            return
        end subroutine 

        subroutine intsp(this,species1, species2, int_sp)
            class(tra), intent(in):: this
            integer:: species1, species2 
            real(dp), intent(out):: int_sp

            integer:: i

            int_sp = Zero
            ! Check if species1 is monotonous
            
            if (any([(this%dat(species1+2, i) - this%dat(species1+2, i-1), i=2, this%nvert)]&
                < -Eps)) then
                call throw_warning(U_out, ' Species 1 in intsp has to be monotonous. &
                    Returning 0')
                return
            end if

            if (this%nvert > 1) then
                do i=2, this%nvert
                    int_sp = int_sp + (this%dat(species1+2, i) - &
                        this%dat(species1+2, i-1))*0.5*&
                        (this%dat(species2+2, i) - this%dat(species2+2, i-1))
                end do
            end if

            int_sp=abs(int_sp)
            return
        end subroutine 

        subroutine minmax(this, tra_min,tra_max)
            class(tra), intent(in):: this
            real(dp), allocatable, intent(out):: tra_min(:), tra_max(:)

            integer:: i

            tra_min = [( minval(this%dat(i,:)), i=1, this%nvert)]
            tra_max = [( maxval(this%dat(i,:)), i=1, this%nvert)]

            return
        end subroutine

        subroutine inivar(trajectory)
            class(tra), intent(inout):: trajectory

            ! Check if trajectory vars initialized
            if (.not.allocated(trajectory%time) .or. .not.allocated(trajectory%dat))&
                call throw_error(U_out, 'Trying to add initial solution to not initialized&
                trajectory')

            return
        end subroutine

        subroutine add_initial_point(trajectory)
            class(tra), intent(inout):: trajectory

            integer:: errl
            real(dp):: cpi(Mnspe), cpden(Mnspe), smf(Neq)
            real(dp), allocatable:: hi(:)
            ! Check if initial profile is allocated
            if (.not.allocated(Inipro)) call throw_error(U_out, 'Initial profile has to be &
                defined and read from fort.3 before it can be added to the trajectory.')

            ! Increase number of points in trajectory and check if it is more, than the 
            ! maximum number of points allowed
            trajectory%nvert = trajectory%nvert + 1
            if (trajectory%nvert > trajectory%nm_nvert) call throw_error(U_out, 'Too many &
                points in trajectory...')

            allocate(hi(Nspec))
            ! Calculate enhtalpy from temperature
            
            trajectory%dat(1:Neq,trajectory%nvert) = Smf0

            return
        end subroutine


        function is_initialized(trajectory) result(is_ini)
            class(tra), intent(in):: trajectory
            logical:: is_ini

            return
        end function 
            
end module homtra
