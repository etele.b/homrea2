C>   hominp1
      SUBROUTINE HOICON(NS,NE,NOPT,NSYMB,SYME,NFILE2,NFILE6,NFILE3,
     1 HCPL,HCPH,HINF,XMOL,NECO,ZJ,V,XI,T,PI,PSI,VI,TIM,XIPR,
     1 NCOMPE,ELEMEN,MNPOI,MNZONE,STOP)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *                                                           *    C
C     *    PROGRAM FOR THE IN/AND OUTPUT OF REACTION CONDITIONS   *    C
C     *                                                           *    C
C     *            LAST CHANGE: 02.10.2005 / A.SCHUBERT           *    C
C     *                                                           *    C
C     *                                                           *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     TSTA     TIME FOR BEGIN OF INTEGRATION                           C
C     TEND     END TIME OF INTEGRATION                                 C
C     P        PRESSURE (BAR)                                          C
C     V        VOLUME                                                  C
C     T        TEMPERATURE                                             C
C     NT       NUMBER OF TIME STEPS                                    C
C     RTOL     RELATIVE TOLERANCE ( 1.E-3   IF NOT SPECIFIED )         C
C     ATOL     ABSOLUTE TOLERANCE ( 1.E-10        "          )         C
C     STEP     INITIAL STEPSIZE   ( 1.E-11        "          )         C
C     TSUR     CONSTANT FOR LOSS OF ENERGY                             C
C     C                  "                                             C
C                                                                      C
C     CALC     NUMBER OF CALCULATIONS                                  C
C                                                                      C
C                                                                      C
C**********************************************************************C
C**********************************************************************C
C     CHAPTER I: STORAGE ORGANIZATION                                  C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL STOP,SWITCH,ELEMEN,EQUSYM,LTIM,LMIX
      PARAMETER(LINLEN=255)
      CHARACTER(LEN=LINLEN)  STRING   
      CHARACTER*2 SYME(*)
      CHARACTER*4 K(9),SYM2
      CHARACTER(LEN=*)  NSYMB(*)
      PARAMETER(LENCIN=33)
      CHARACTER(LEN=LENCIN)  MSYMB(NS),ISYM     
      CHARACTER*20 MM
      DIMENSION NOPT(*),SWITCH(9),V(9),ZJ(*)
      DIMENSION HCPL(7,NS),HCPH(7,NS),HINF(3,NS),XMOL(NS)
      DIMENSION XI(NS),XIPR(*),WUEGR(NS)
      DIMENSION XMIX(NS)
      DIMENSION T(*),TIM(3,*),VI(*),PI(*),PSI(*)
      DIMENSION NCOMPE(NS,*)
      DIMENSION XIR(MNZONE,NS)
      DOUBLE PRECISION, DIMENSION(MNZONE):: TR,VIR,AFRAMU,FRACT
      DOUBLE PRECISION, DIMENSION(MNZONE+1):: ALPHA
      DATA K(1)/'TEND'/,K(2)/'NT  '/,K(3)/'ATOL'/,K(4)/'RTOL'/,
     1     K(5)/'STEP'/,K(6)/'TSTA'/,K(7)/'@@@@'/,K(8)/'ATOS'/,
     1     K(9)/'RTOS'/
      DATA MMAX/10/
C**********************************************************************C
C     CHAPTER I:  FIND FIRST CONDITION HEADING                         C
C**********************************************************************C
      LENSYM=LEN(NSYMB(1))
      NC=1
   10 READ(NFILE2,210,END=9000) MM
      IF(.NOT.EQUSYM(4,MM,'COND')) GOTO 10
      GOTO 1101
C**********************************************************************C
C     IV.1: INPUT OF CONDITIONS HEADING                                C
C**********************************************************************C
  205 CONTINUE
  210 FORMAT(A20)  
      READ(NFILE2,210,END=1000) MM      
      IF(EQUSYM(4,MM,'****')) GOTO 205
C---- ERROR EXIT
      IF(.NOT.EQUSYM(4,MM,'COND')) GOTO 1000
      NC=NC+1
C*********************************************************************C
C     IV.2: INITIALIZATION                                            C
C*********************************************************************C
 1101 CONTINUE
C---- NUMBERS
      IM=0
      IMM=0
      NPOV=0
      NPOP=0
      NPOT=0
CASSA NUMBER OF REACTORS    
      NUR=NOPT(32)      
C---- MOLE FRACTIONS
      XI(1:NS)=0.0
      XMIX(1:NS)=0.0
C---- INTEGRATION PARAMETERS
      DO 302 I=1,9
      V(I)=0.0
  302 SWITCH(I)=.FALSE.
      V(3)=1.0E-10
      V(4)=1.0E-3
      V(5)=1.0E-11
      V(8)=1.0E-9
      V(9)=2.0E-3
C---- PHYSICAL CONDITIONS
      VI(1:MNPOI)=0.0
      PI(1:MNPOI)=0.0
      T(1:MNPOI)=0.0
      NHINIT=0
      NHMIX=0
      HINIT=0.D0
      DNDTIN=0.D0
      DNDTIN=0.D0
C**********************************************************************C
C     IV.3: INPUT                                                      C
C**********************************************************************C
      NECO=0
  301 CONTINUE
      ISYM=REPEAT(' ',LENCIN)
      READ(NFILE2,'(A)') STRING
      IF(EQUSYM(3,STRING,'END')) GOTO 900
      IF(EQUSYM(4,STRING,'****')) GOTO 301
      IF(SCAN(STRING,'!').GT.0) STRING(SCAN(STRING,'!'):LINLEN)=' '
C**********************************************************************C
C     IV.3: INPUT                                                      C
C**********************************************************************C
      CALL SPLCON(STRING,ISYM,XNM,SYM2,YNM,IERR)
      IF(IERR.GT.0) GOTO 9999
C     IF(SYM2.EQ.'t') SYM2='MIXI'
C     IF(LMIX)      YNM=XMIT    
C     IF(.NOT.LMIX) YNM=TIMA    
C**********************************************************************C
C     IV.3: INPUT                                                      C
C**********************************************************************C
C---- VOLUME
      IF(EQUSYM(4,ISYM,'V   ')) THEN
        NPOV=NPOV+1
        VI(NPOV)=XNM
        TIM(2,NPOV)=YNM
        GOTO 301
      ENDIF
C---- PRESSURE
      IF(EQUSYM(4,ISYM,'P   ')) THEN 
        NPOP=NPOP+1
        PI(NPOP)=XNM
        TIM(1,NPOP)=YNM
        GOTO 301
      ENDIF
C---- TEMPERATURE
************************************************************************
      IF(EQUSYM(4,ISYM,'T   ').AND.(.NOT.EQUSYM(4,SYM2,'MIXI'))) THEN 
        NPOT=NPOT+1
        T(NPOT)=XNM
        TIM(3,NPOT)=YNM
        GOTO 301
      ENDIF
      IF(EQUSYM(4,ISYM,'T   ').AND.(EQUSYM(4,SYM2,'MIXI'))) THEN 
        TMIX=XNM
        GOTO 301
      ENDIF
      IF(EQUSYM(4,ISYM,'H(T0').AND.(.NOT.EQUSYM(4,SYM2,'MIXI'))) THEN      
        NHINIT=1
        HINIT=XNM
        GOTO 301
      ENDIF
      IF(EQUSYM(4,ISYM,'H(T0').AND.(EQUSYM(4,SYM2,'MIXI'))) THEN      
        NHMIX =1
        HMIX =XNM
        GOTO 301
      ENDIF
C---- GAS COMPOSITION
      DO 320 J=1,NS
      IF(EQUSYM(LENSYM,ISYM,NSYMB(J)).AND.
     1        (.NOT.EQUSYM(4,SYM2,'MIXI'))) THEN
        XI(J)=XNM
        IM=IM+1
        GOTO 301
      ENDIF
      IF(EQUSYM(LENSYM,ISYM,NSYMB(J)).AND.
     1        (EQUSYM(4,SYM2,'MIXI'))) THEN
        XMIX(J)=XNM
        IMM=IMM+1
        GOTO 301
      ENDIF
  320 CONTINUE
C---- ELEMENT COMPOSITION
      IF(ELEMEN) THEN
      DO 325 J=1,NE
      IF(EQUSYM(4,ISYM,'ELE.').AND.EQUSYM(2,ISYM(5:8),SYME(J))) THEN
        NECO=NECO+1
        ZJ(J)=XNM
        WRITE(6,*) XNM, SYME(J)
        GOTO 301
      ENDIF
  325 CONTINUE
      ENDIF
      IF(EQUSYM(7,ISYM,'DNDT_in')) THEN      
        DNDTIN=XNM
        GOTO 301
      ENDIF
      IF(EQUSYM(8,ISYM,'DNDT_out')) THEN      
        DNDTOU=XNM
        GOTO 301
      ENDIF
C---- INTEGRATION PARAMETERS
      DO 330 I=1,9
      IF(EQUSYM(4,ISYM,K(I))) THEN      
        SWITCH(I)=.TRUE.
        V(I)=XNM
        GOTO 301
      ENDIF
  330 CONTINUE
C---- IF WE ARE HERE UNKNOWN SYMBOL IS DETECTED
      GOTO 9100
  900 CONTINUE
C**********************************************************************C
C
C     IV.: CHECKS                                                      C
C
C**********************************************************************C
C**********************************************************************C
C     IV.1 : CHECK FOR COMPLETENESS OF DATA                            C
C**********************************************************************C
C---- CONDITIONS
      STOP=.TRUE.
      KV=2
      DO 400 I=1,KV
        STOP=STOP.AND.SWITCH(I)
  400 CONTINUE
      IF(.NOT.STOP) GOTO 9210
C-----INITIAL P,V
      IF((NPOP.LE.0).OR.(NPOV.LE.0)) GOTO 9220
      IF((NPOT.LE.0).AND.NOPT(2).EQ.1)GOTO 9222
      IF((NPOT.LE.0).AND.(NHINIT.LE.0))GOTO 9224
      IF((NPOT.GT.0).AND.(NHINIT.GT.0))GOTO 9226
      IF(NOPT(7).EQ.0.AND.(NHINIT.GT.0))GOTO 9228
C---- NUMBER OF SPECIES
      IF(IM.GT.NS.OR.IMM.GT.NS) GOTO 9230
C**********************************************************************C
C     CHECK FOR MONOTONICY                                             C
C**********************************************************************C
      DO 420 KK=1,3
      IF(KK.EQ.1) NFIN=NPOP-1
      IF(KK.EQ.2) NFIN=NPOV-1
      IF(KK.EQ.3) NFIN=NPOT-1
      IF(NFIN.LE.0) GOTO 420
      DO 410 MOC=1,NFIN
      IF(TIM(KK,MOC).GE.TIM(KK,MOC+1)) GOTO 9235
  410 CONTINUE
  420 CONTINUE
C**********************************************************************C
C     IV.5: NORMALIZATION OF INITIAL MIXTURE                           C
C**********************************************************************C
      SUM=0.0
      DO 510 I=1,NS
      SUM=SUM+XI(I)
  510 CONTINUE
      DO 520 I=1,NS
      XI(I)=XI(I)/SUM
  520 CONTINUE
C**********************************************************************C
C     NORMALIZATION OF MIXING COMPOSITION  
C**********************************************************************C
      SUM=0.0
      DO I=1,NS
      SUM=SUM+XMIX(I)
      ENDDO
      XMIX(1:NS)=XMIX(1:NS)/SUM
C**********************************************************************C
C     IV.5: NORMALIZATION OF ATOM MOLE FRACTIONS                       C
C**********************************************************************C
      IF(ELEMEN) THEN
C---- CHECK FOR COMPLETENESS
        IF(NECO.EQ.NE) THEN
C---- ALL ATOM MOLE FRACTIONS HAVE BEEN SPECIFIED, NORMALIZE
          SUM=0.0
          DO 540 J=1,NE
          SUM=SUM+ZJ(J)
  540     CONTINUE
          DO 550 J=1,NE
          ZJ(I)=ZJ(I)/SUM
  550     CONTINUE
        ELSE
C---- NO ATOM MOLE FRACTIONS HAVE BEEN SPECIFIED, CALCULATE THEM
          IF(NECO.EQ.0) THEN
            SUM=0.D0
            DO 570 J=1,NE
            ZJ(J)=0.D0
            DO 560 I=1,NS
            ZJ(J)=ZJ(J)+FLOAT(NCOMPE(I,J))*XI(I)
  560       CONTINUE
            SUM=SUM+ZJ(J)
  570       CONTINUE
            DO 580 J=1,NE
            ZJ(J)=ZJ(J)/SUM
C           WRITE(6,*) ZJ(J)
  580       CONTINUE
          ELSE
            GOTO 9213
          ENDIF
        ENDIF
      ENDIF
C**********************************************************************C
C     IV.6: CALCULATE INITIAL TEMPERATURE FOR SPECIFIC ENTHALPY GIVEN  C
C**********************************************************************C
      IF(NHINIT.GT.0) THEN
      CALL GETT(NS,HINIT,XI,HCPL,HCPH,HINF,XMOL,TH,NFILE6)
      NPOT=1
      TIM(3,1)=0.D0 
      T(1) = TH
      ENDIF
      IF(NHMIX.GT.0) THEN
      CALL GETT(NS,HMIX,XMIX,HCPL,HCPH,HINF,XMOL,TMIX,NFILE6)
      ENDIF
C---- READ AND CHECK INPUT DATA FOR WALL HEAT TRANSFER COMPUTATION   
C A. Schubert, 11/08/2005                                      
      IF(NOPT(4) .GT. 0) THEN
        CALL WALCON(NFILE6,NFILE2,TSURR,SURVOL,CHT1,CHT2)
      ENDIF      
C**********************************************************************C
C     IV.7.1: READ AND CHECK INPUT DATA FOR ENGINE CYCLE COMPUTATION   C
C             (A. Schubert, 11/08/2005)                                C
C**********************************************************************C
      IF(NOPT(5) .GT. 0) THEN        
        CALL ENGCON(NFILE2,NFILE6,NOPT,NSYMB,NS,XMOL,T(1),XI,NEGR,
     1              WUEGR,TEGR,PEGR,EGRM,HCPL,HCPH,HINF)      
      ENDIF     
C**********************************************************************C
C     IV.7.2: READ AND CHECK INPUT DATA FOR MULZON CYCLE COMPUTATION   C
C             (A. Schubert, 02/10/2005)                                C
C**********************************************************************C
      IF(NOPT(32) .GT. 0 .OR. NOPT(4).GT.0) THEN        
        CALL MULCON(NFILE2,NFILE6,NOPT,NSYMB,NS,NUR,MNZONE,XMOL,CHT1,
     1        CHT2,T(1),VI(1),XI,TR,VIR,XIR,AFRAMU,ALPHA,FRACT,NEGR,
     1        WUEGR,TEGR,HCPL,HCPH,HINF)    
      ENDIF      
C**********************************************************************C
C     IV.8: OUTPUT OF INITIAL VALUES ON FILE NFILE3                    C
C**********************************************************************C
C---- TRANSFORM PRESSURE TO SI UNITS
      DO 720 I=1,NPOP
      PSI(I)=PI(I)*1.0D5
  720 CONTINUE
C**********************************************************************C
C     OUTPUT OF POINTS ON NFILE 3                                      C
C**********************************************************************C
  721 FORMAT(1X,I4,1X,I4,1X,I4,' (NPRESS,NVOL,NTEMP)')
      WRITE(NFILE3,721) NPOP,NPOV,NPOT
 1664 FORMAT(1X,'P(PA)   :  ',1PE9.3,3X,'AT TIME = ',1PE9.3)
      WRITE(NFILE3,1664) (PSI(J),TIM(1,J),J=1,NPOP)
 1665 FORMAT(1X,'V(REL.) :  ',1PE9.3,3X,'AT TIME = ',1PE9.3)
      WRITE(NFILE3,1665) (VI(J),TIM(2,J),J=1,NPOV)
 1666 FORMAT(1X,'T(K)    :  ',1PE9.3,3X,'AT TIME = ',1PE9.3)
      WRITE(NFILE3,1666) (T(J),TIM(3,J),J=1,NPOT)
C---- SPECIES
  760 FORMAT(' XU  ',7X,5(1PE20.13,1X))
      WRITE(NFILE3,760) (XI(I),I=1,NS)          
C---- FIRST AND LAST TIME FOR OUTPUT , NUMBER OF TIME STEPS
C     IF(V(2).GT.60.0) V(2)=60.
      NUT  =IDINT(V(2)     +1.D-5)
  770 FORMAT(' TB,TE,NT   ',5(1PE11.4,1X))
      WRITE(NFILE3,770) V(6),V(1),V(2)
C---- ERROR TOLERANCES AND INITIAL STEPSIZE
  796 FORMAT(' AT,RT,ST',3X,5(1PE11.4,1X))
      WRITE(NFILE3,796) V(3),V(4),V(5),V(8),V(9)
C********************************************************
C********************************************************
C_A. Schubert, 11/08/2005) 
C---- CONST. FOR STIRRED REACTOR
      IF (NOPT(4).GT.0) THEN
        WRITE(NFILE3,790) TSURR,SURVOL,CHT1,CHT2 
  790 FORMAT(' TS,SV,CH',3X,5(1PE11.4,1X))
      ENDIF
      IF (NOPT(6).GT.0) THEN
        WRITE(NFILE3,791) DNDTIN,DNDTOU,TMIX,XMIX(1:NS)   
  791 FORMAT(' PSR     ',3X,5(1PE11.4,1X))
      ENDIF
C----   OUTPUT DATA FOR MULTIZONE COMPUTATION ON NFILE3                                              
      IF(NOPT(32).GT.0 .OR. NOPT(4).GT.0) THEN
        CALL MULOUT(NUR,NS,NOPT,MNZONE,TR,VIR,XIR,AFRAMU,ALPHA,NFILE3)
      ENDIF
C----  OUTPUT DATA FOR ENGINE CYCLE COMPUTATION ON NFILE3      
      IF(NOPT(5).GT.0) THEN
        CALL ENGOUT(NFILE3,NFILE6,NOPT)           
      ENDIF      
C_end A. Schubert  
C**********************************************************************C
C     IV.7: OUTPUT ON FILE NFILE6                                      C
C**********************************************************************C
C---- OUTPUT OF HEADING
      WRITE(NFILE6,1)
    1 FORMAT(1X)
  661 FORMAT(1X,18('*'),3X,'Output of Data for Calculation No.',
     1              I3,3X,18('*'))
      WRITE(NFILE6,661) NC
      WRITE(NFILE6,1)
      IF(NOPT(32) .GT. 0) THEN 
        CALL MULOU2(NUR,NS,NSYMB,MNZONE,XIR,PI,VI,VIR,TR,NFILE6,
     1              AFRAMU,ALPHA,FRACT,NOPT,NPOP,NPOV,TIM)
      ELSE
C---- GAS COMPOSITION
  662   FORMAT(8X,'Gas Composition (mole fractions)')
C 672 FORMAT(1H ,7X,'GAS COMPOSITION (CONCENTRATIONS IN MOLE/M**3)')
        WRITE(NFILE6,662)
C     IF(NOPT(6).NE.0) WRITE(NFILE6,672)
  663   FORMAT(1(8X,3(A,':',1PE9.2,3X)))
C---- Extract non-zero mole fraction for print-out
        IPR=0
        DO 2828 I=1,NS
          IF(XI(I).LE.0.0D0) GOTO 2828
          IPR=IPR+1
          MSYMB(IPR)(1:LENSYM)=NSYMB(I)
          XIPR(IPR)   =XI   (I  )
 2828   CONTINUE
C---- Print non-zero mole fractions
        WRITE(NFILE6,663) (MSYMB(J)(1:LENSYM),XIPR(J),J=1,IPR)
        WRITE(NFILE6,1)
C---- VALUES OF T,P,V
    2   FORMAT(8X,'Values of T(K), P(bar), and V(rel.) ',
     1         'at Different Times')
        WRITE(NFILE6,2)
  664   FORMAT(8X,'P : ',1PE9.3,' bar      at t = ',1PE9.3,' s')
        WRITE(NFILE6,664) (PI(J),TIM(1,J),J=1,NPOP)
        IF((NPOV.GT.1).OR.(NPOP.GT.1)) WRITE(NFILE6,1)
  665   FORMAT(8X,'V : ',1PE9.3,' (-)      at t = ',1PE9.3,' s')
        WRITE(NFILE6,665) (VI(J),TIM(2,J),J=1,NPOV)
        IF((NPOT.GT.1).OR.(NPOV.GT.1)) WRITE(NFILE6,1)
  666   FORMAT(8X,'T : ',1PE9.3,' K        at t = ',1PE9.3,' s')
        WRITE(NFILE6,666) (T(J),TIM(3,J),J=1,NPOT)
        WRITE(NFILE6,1)
C 667 FORMAT(1H ,7X,'NPOP= ',I2,'   NPOV= ',I2,'   NPOT= ',I2)
C     WRITE(NFILE6,667) NPOP,NPOV,NPOT
      IF(NOPT(6).GT.0) THEN
C---- GAS COMPOSITION
  762   FORMAT(8X,'Mixing composition (mole fractions)')
        WRITE(NFILE6,762)
C---- Extract non-zero mole fraction for print-out
        IPR=0
        DO 2829 I=1,NS
          IF(XMIX(I).LE.0.0D0) GOTO 2829
          IPR=IPR+1
          MSYMB(IPR)(1:LENSYM)=NSYMB(I)
          XIPR(IPR)   =XMIX (I  )
 2829   CONTINUE
C---- Print non-zero mole fractions
        WRITE(NFILE6,663) 'dM/dt_in',DNDTIN,'dM/dt_ou',DNDTOU,
     1    'Tmix    ',TMIX
        WRITE(NFILE6,663) (MSYMB(J)(1:LENSYM),XIPR(J),J=1,IPR)
        ENDIF
C***********************************************************************
C
C***********************************************************************
        WRITE(NFILE6,1)
      ENDIF
C---- POLYNOMS
C---- OUTPUT OF TIME STEPS
  610 FORMAT(8X,'Number of time steps        =',I10)
      WRITE(NFILE6,610) NUT
C---- OUTPUT OF BEGIN OF INTEGRATION
  615 FORMAT(8X,'Time of first output        =',E10.3,' s ')
      V8=V(8)*1000.0
      WRITE(NFILE6,615) V(8)
C---- OUTPUT OF END TIME OF INTEGRATION
  620 FORMAT(8X,'End time of integration     =',E10.3,' s ')
      V1=V(1)*1000.0
      WRITE(NFILE6,620) V(1)
      IF(NOPT(4).EQ.1) THEN
C---- OUTPUT OF TSURR, CONST
        WRITE(NFILE6,630) TSURR
  630   FORMAT(8X,'Temperature of surrounding  =',E10.3,' K ')
        WRITE(NFILE6,640) CHT1 
  640   FORMAT(8X,'Constant in Newtons law     =',E10.3,' J/K*s?')
      ENDIF
C---- OUTPUT OF INTEGRATION PARAMETERS
  696 FORMAT(8X,'Relative error tolerance    =',E10.3)
  697 FORMAT(8X,'Absolute error tolerance    =',E10.3)
  698 FORMAT(8X,'Initial stepsize            =',E10.3,' s ')
      WRITE(NFILE6,697) V(3)
      WRITE(NFILE6,696) V(4)
      WRITE(NFILE6,698) V(5)
CASSA  
      IF(NOPT(5).GT.0) THEN
        CALL ENGOU2(NFILE6,NOPT)
      ENDIF    
  999 CONTINUE
      GOTO 205
 1000 RETURN
C**********************************************************************C
C     ERROR EXITS                                                      C
C**********************************************************************C
 9001 FORMAT(6X,'ERROR - No reaction conditions specified')
 9000 WRITE(NFILE6,9001)
      GOTO 9999
C9010 WRITE(NFILE6,9011) (MM(I),I=1,6)
C9011 FORMAT(1H ,5X,'WRONG HEADING OF CONDITIONS : ',6A4)
C     GOTO 9999
 9100 WRITE(NFILE6,9101) ISYM(1:8)
 9101 FORMAT(1H1,5X,'ERROR - Wrong symbol in reaction conditions : ',
     F       A,'...')
      GOTO 9999
 9210 WRITE(NFILE6,9211)
 9211 FORMAT(1H1,5X,'ERROR - Data on reaction conditions incomplete')
      GOTO 9999
 9220 WRITE(NFILE6,9221)
 9221 FORMAT(1H1,5X,'ERROR - Initial P or V not specified')
      GOTO 9999
 9222 WRITE(NFILE6,9223)
 9223 FORMAT(1H1,5X,'ERROR - No initial T specified with option TPRO')
      GOTO 9999
 9213 WRITE(NFILE6,9214)
 9214 FORMAT('1 ','ERROR - Inconsistency in specified atom composition')
      GOTO 9999
 9224 WRITE(NFILE6,9225)
 9225 FORMAT(1H1,5X,'ERROR - Neither initial T nor initial enthalpy ',/,
     1       '1',5X,'        specified for adiabatic reactor        ')
      GOTO 9999
 9226 WRITE(NFILE6,9227)
 9227 FORMAT(1H1,5X,'ERROR - Initial T and initial enthalpy ',/,
     1       '1',5X,'        specified for adiabatic reactor        ')
      GOTO 9999
 9228 WRITE(NFILE6,9229)
 9229 FORMAT(1H1,5X,'ERROR - Initial ENTHALPY only valid together ',/,
     1       '1',5X,'        with option ELEM                       ')
      GOTO 9999
 9230 WRITE(NFILE6,9231) NS,IM,IMM
 9231 FORMAT(1H1,5X,'ERROR - More than ',I2,
     1 ' initial values defined, IM=',I5,', IMM =',I5)
      GOTO 9999
 9235 GOTO(9240,9250,9260),KK
 9240 WRITE(NFILE6,9241)
 9241 FORMAT(1H ,5X,'ERROR - Values of pressure out of order')
      GOTO 9999
 9250 WRITE(NFILE6,9251)
 9251 FORMAT(1H ,5X,'ERROR - Values of volume out of order')
      GOTO 9999
 9260 WRITE(NFILE6,9261)
 9261 FORMAT(1H ,5X,'ERROR - Values of temperature out of order')
      GOTO 9999
 9999 CONTINUE
      WRITE(NFILE6,9998)
 9998 FORMAT(1H ,5X,'ERROR - Wrong operation of HOICON')
      STOP=.FALSE.
      RETURN
C**********************************************************************C
C     END OF HOICON                                                    C
C**********************************************************************C
      END
      SUBROUTINE REDMEC(NS,NSYMB,NRMAX,NR,KOEFL,KOEFR,IREVER,
     1   INPF,NFILE3,NFILE6,IWORK,STOP,TEST,TSO,
     2   NCS,NPE,NQS,NZS,ICS)
C***********************************************************************
C                                                                      *
C     ************************************************************     *
C     *                                                          *     *
C     *      Input and Computation of Information for Conven-    *     *
C     *             tional reduction of a mechanism              *     *
C     *                                                          *     *
C     ************************************************************     *
C                                                                      *
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL STOP,TEST,TSO
      CHARACTER(LEN=*) NSYMB(*)
      DIMENSION KOEFL(NR,*),KOEFR(NR,*),IREVER(*)
      DIMENSION IWORK(*),ICS(*)
      CHARACTER*4 PESYMB(3),QSSYMB(3),ZCSYMB(3),CSSYMB(3)
C----
      DATA NPEH/3/,PESYMB/'PART','part','Part'/
      DATA NCSH/3/,CSSYMB/'FIXE','fixe','Fixe'/
      DATA NQSH/3/,QSSYMB/'QUAS','quas','Quas'/
      DATA NZCH/3/,ZCSYMB/'ZERO','zero','Zero'/
C**********************************************************************C
C     Initialization                                                   C
C**********************************************************************C
      TEST=.TRUE.
      STOP=.TRUE.
C***********************************************************************
C
C     Block for data of conventionally reduced mechanism
C
C***********************************************************************
      IPOI=1
C***********************************************************************
C     Input of species assumed to be in partial equilibrium            C
C***********************************************************************
      CALL HOICSP(NQSH,QSSYMB,INPF,NFILE6,NS,NSYMB,NS,NQS,ICS(IPOI))
      WRITE(NFILE6,84)  (NSYMB(ICS(IPOI-1+J)),J=1,NQS)
   84 FORMAT(' ',7X,'List of species that are assumed to be in quasi-',
     1   'steady state:',1(/,5(3X,A)))
      IPOI=IPOI+NQS
C***********************************************************************
C     Input of species assumed not be be present (w=0)                 C
C***********************************************************************
      CALL HOICSP(NZCH,ZCSYMB,INPF,NFILE6,NS,NSYMB,NS,NZC,ICS(IPOI))
      IF(NZC.GT.0) WRITE(NFILE6,86)  (NSYMB(ICS(IPOI-1+J)),J=1,NZC)
   86 FORMAT(' ',7X,'List of species with w=0   :',1(/,5(3X,A)))
      IPOI=IPOI+NZC
C***********************************************************************
C     reactions in partial equilibrium
C***********************************************************************
C---- INPUT OF REACTIONS
      CALL GENREV(NPEH,PESYMB,NS,NSYMB,INPF,NFILE6,IWORK,NUMPE,
     1    STOP,TEST,TSO)
      IF(.NOT.STOP) GOTO 999
C---- IWORK CONTAINS THE REACTION VECTORS IN ITS COLUMNS
C***********************************************************************
C     IDENTIFY REACTION VECTORS AS ELEMENTARY REACTIONS                C
C***********************************************************************
C---- GENERATE MATRIX FROM KOEFL,KOEFR
C     DO 510 J=1,NR
C     DO 510 I=1,NS
C     IWORK(NUMPE*NS+I+(J-1)*NS) = KOEFL(J,I) + KOEFR(J,I)
C 510 CONTINUE
C---- COMPARE COEFFICIENTS OF REACTION K WITH ELEMENTARY REACTION J
      NPE=0
      IF(NUMPE.GT.0) THEN
      DO 550 K=1,NUMPE
      DO 540 J=1,NR
C---- GENERATE MATRIX FROM KOEFL,KOEFR
      DO 510 I=1,NS
      IWORK(NUMPE*NS+I) = KOEFL(J,I) + KOEFR(J,I)
  510 CONTINUE
      ISUM=0
      DO 530 I=1,NS
C     IELRE= NUMPE * NS +  I + (J-1) * NS
      IELRE= NUMPE * NS +  I
      IREVA=               I + (K-1) * NS
      ISUM=ISUM + IABS(IWORK(IELRE)-IWORK(IREVA))
  530 CONTINUE
      IF(ISUM.EQ.0) THEN
C---- IF WE ARE HERE, REACTION K CORRESPONDS TO ELEMENTARY REACTION J
C     WRITE(6,*) ' HEUREKA ', K , J
C---- SET UP INDEX OF ELEMENTARY REACTION
      IF(IABS(IREVER(J)).EQ.2) THEN
        INDEX = J
      ELSE
        IF(IABS(IREVER(J)).EQ.3) THEN
          INDEX = J-1
        ELSE
C---- ELEMENTARY REACTION J IS NO REVERSIBLE REACTION
          GOTO 9520
        ENDIF
      ENDIF
      NPE=NPE+1
      IF(NPE.GT.NS) THEN
          WRITE(6,*) '  --- ERROR ---'
          STOP
        ENDIF
      ICS(IPOI-1 +NPE)=INDEX
C---- LEAVE LOOP OVER ELEMENTARY REACTIONS
      GOTO 550
      ENDIF
  540 CONTINUE
C---- IF WE ARE HERE, A REACTION DEFINED TO BE IN PART.EQ. IS NOT EL.
      GOTO 9530
  550 CONTINUE
      ENDIF
C***********************************************************************
C     OUTPUT ON NFILE3
C***********************************************************************
C----
      WRITE(NFILE3,827) NPE
      IF(NPE.GT.0)  WRITE(NFILE3,837) (ICS(I+NQS+NZC),I=1,NPE)
C----
      WRITE(NFILE3,828) NQS
      IF(NQS.GT.0)  WRITE(NFILE3,837) (ICS(I),I=1,NQS)
C----
      WRITE(NFILE3,829) NZC
      IF(NZC.GT.0)  WRITE(NFILE3,837) (ICS(I+NQS),I=1,NZC)
  827 FORMAT(I3,' R. IN P.E. :')
  828 FORMAT(I3,' QSS. SPEC. :')
  829 FORMAT(I3,' ZERO SPEC. :')
  837 FORMAT(15(I3))
      RETURN
C***********************************************************************
C     ERROR EXITS
C***********************************************************************
  999 CONTINUE
      STOP=.FALSE.
      WRITE(NFILE6,998)
  998 FORMAT(3(/,' +++++  ERROR IN REDMEC  +++++ '))
      RETURN
 9520 CONTINUE
      WRITE(NFILE6,9521) K
 9521 FORMAT(' ',' Error: Reaction',I3,', which is assumed to be in',
     1           ' partial equilibrium does correspond to an',/,
     1       ' ',' elementary reaction that is not reversible')
      GOTO 999
 9530 CONTINUE
      WRITE(NFILE6,9531) K
 9531 FORMAT(' ',' Error: Reaction',I3,', which is assumed to be in',
     1           ' partial equilibrium does not correspond',/,
     1       ' ',' to any elementary reaction')
      GOTO 999
C***********************************************************************
C     END OF REDMEC
C***********************************************************************
      END
      SUBROUTINE GENREV(NH,HSYMB,NS,NSYMB,INPF,NFILE6,
     2      KOEF,NR,STOP,TEST,TSO)
C**********************************************************************C
C                                                                      C
C    ***************************************************************   C
C    *                                                             *   C
C    *  PROGRAM FOR THE INPUT AND PRINTOUT OF REACTION VECTORS     *   C
C    *                                                             *   C
C    ***************************************************************   C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     INPUT  :                                                         C
C                                                                      C
C     NRMAX          = MAXIMUM NUMBER OF REACTIONS (FIRST INDEX OF     C
C                      KOEFR AND KOEFL)                                C
C     NS             = NUMBER OF SPECIES                               C
C     NSYMB(2,*)     = SPECIES SYMBOLS (NS+NM)                         C
C     INPF           = NUMBER OF INPUT FILE                            C
C     NFILE6         = NUMBER OF OUTPUT FILE FOR LISTING               C
C                                                                      C
C     OUTPUT :                                                         C
C                                                                      C
C     INERT(*)       = INERT SPECIES NUMBERS (NS)                      C
C     NINERT         = NUMBER OF INERT SPECIES                         C
C     IREVER(*)      = VECTOR FOR THE IDENTIFICATION OF REVERSE REAC-  C
C                      TIONS (NRMAX)                                   C
C     KOEFR          = MATRIX OF RIGHT AND LEFT HAND SIDE STOICHIOME-  C
C                      TRIC COEFFICIENTS (NRMAX,NSNM)                  C
C     NR             = NUMBER OF REACTIONS                             C
C     STOP           = .TRUE. FOR NORMAL END, .FALSE. FOR ABNORMAL END C
C                                                                      C
C                                                                      C
C**********************************************************************C
C                                                                      C
C                                                                      C
C                                                                      C
C**********************************************************************C
C     STORAGE ORGANIZATION                                             C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL EQUSYM
      LOGICAL STOP,TEST,TSO,FINISH,NOMECH
      CHARACTER*4 ITEXT(18),HSYMB(NH)
      CHARACTER(LEN=*) NSYMB(*)
      DIMENSION   KOEF(NS,*)
C**********************************************************************C
C     INITIALIZATION
C**********************************************************************C
      STOP=.TRUE.
C**********************************************************************C
C     INPUT,OUTPUT OF HEADING                                          C
C**********************************************************************C
      REWIND INPF
   10 READ(INPF,801,END=900,ERR=900) (ITEXT(I),I=1,18)
      DO 15 I=1,NH
      IF(EQUSYM(4,ITEXT(1),HSYMB(I))) GOTO 17
   15 CONTINUE
      GOTO 10
   17 CONTINUE
C**********************************************************************C
C
C     INPUT OF REACTIONS                                               C
C
C**********************************************************************C
      NR=0
  100 CONTINUE
      NR=NR+1
      CALL INRVEC(NS,NSYMB,INPF,NFILE6,KOEF(1,NR),
     1                     STOP,TEST,TSO,FINISH)
      IF(.NOT.STOP) GOTO 999
      IF(FINISH) NR=NR-1
      IF(FINISH) GOTO 400
C***********************************************************************
C     RECOGNIZE IDENTICAL REACTIONS
C***********************************************************************
      IF(NR.EQ.1) GO TO 290
      NRM1=NR-1
      DO 240 JJ=1,NRM1
      NRL=0
      NRR=0
      DO 230 LL=1,NS
      IF(KOEF(LL,NR).EQ.KOEF(LL,JJ)) NRL=NRL+1
  230 CONTINUE
      IF( NRL.EQ.NS ) WRITE(NFILE6,821) NR,JJ
  240 CONTINUE
  290 CONTINUE
      GO TO 100
C**********************************************************************C
C     END OF INPUT OF REACTIONS                                        C
C**********************************************************************C
  400 CONTINUE
      RETURN
C**********************************************************************C
C     ERROR EXITS                                                      C
C**********************************************************************C
 900  WRITE(NFILE6,901) (HSYMB(I),I=1,NH)
 901  FORMAT('1',1X,' No input received for reaction vectors,',
     1       ' possible headings are:',/,10(2X,A4))
      GOTO 999
 999  STOP=.FALSE.
      RETURN
C**********************************************************************C
C     FORMAT STATEMENTS                                                C
C**********************************************************************C
  801 FORMAT(18A4)
  821 FORMAT('0',7X,'WARNING:    REACTION ',I3,' IS EQUAL TO REACTION ',
     1       I3)
C**********************************************************************C
C     END OF -HOIMEC-                                                  C
C**********************************************************************C
      END
      SUBROUTINE INRVEC(NS,NSYMB,INPF,NFILE6,NSTOE,STOP,TEST,TSO,FINISH)
C**********************************************************************C
C                                                                      C
C    ***************************************************************   C
C    *                                                             *   C
C    *  PROGRAM FOR THE INPUT OF A "REACTION MATRIX"               *   C
C    *                       11.07.1989                            *   C
C    *              LAST CHANGE: 11.07.1989/MAAS                   *   C
C    *                                                             *   C
C    ***************************************************************   C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     INPUT  :                                                         C
C                                                                      C
C     NS             = NUMBER OF SPECIES                               C
C     NM             = NUMBER OF THIRD BODIES                          C
C     NSYMB(*)       = SPECIES SYMBOLS (NS+NM)                         C
C     INPF           = NUMBER OF INPUT FILE                            C
C     NFILE6         = NUMBER OF OUTPUT FILE FOR LISTING               C
C                                                                      C
C     OUTPUT :                                                         C
C                                                                      C
C     FINISH         = .TRUE. IF END OF MECHANISM HAS BEEN DETECTED    C
C     STOP           = .TRUE. FOR NORMAL END, .FALSE. FOR ABNORMAL END C
C     MAT(6)         = MATRIX OF COEFFICIENTS                          C
C                                                                      C
C     INPUT FROM FILE INPF :                                           C
C                                                                      C
C                                                                      C
C**********************************************************************C
C                                                                      C
C                                                                      C
C-CHARACTER only 8 character species are allowed                       C
C**********************************************************************C
C     STORAGE ORGANIZATION                                             C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL EQUSYM,STOP,TEST,TSO,FINISH
      CHARACTER*1 NZS(6)
      CHARACTER*4 NHELP4
      CHARACTER(LEN=*)  NSYMB(*)
      CHARACTER*8 NSC,NSYR(7)
      DIMENSION NSTOE(NS),ISTOE(6),MAT(6)
C**********************************************************************C
C     INITIALIZATION
C**********************************************************************C
      STOP=.TRUE.
      FINISH=.FALSE.
      LENSYM=LEN(NSYMB(1))
      NFILL=LENSYM-8
C**********************************************************************C
C
C     INPUT OF REACTIONS                                               C
C
C**********************************************************************C
  100 CONTINUE
C***********************************************************************
C     READ REACTION NR
C***********************************************************************
      READ(INPF,2424,ERR=910) NHELP4
C---- RECOGNIZE COMMENT LINES, BEGINNING WITH "*"
 2424 FORMAT(A4)
      IF(NHELP4.EQ.'----'.OR.NHELP4.EQ.'****')    GOTO 100
C---- RECOGNIZE END LINE
      IF(EQUSYM(3,NHELP4,'END'))     GOTO 400
C---- IF WE ARE HERE, THIS LINE CONTAINS INFORMATION
      BACKSPACE INPF
      READ(INPF,810,ERR=910)(ISTOE(KK),NSYR(KK),NZS(KK),KK=1,6)
C***********************************************************************
C     CHECK OF SPECIES CONTAINED IN THE MECHANISM
C***********************************************************************
      NLORGL=0
      DO 190 KK=1,6
      NSC=NSYR(KK)
         MAT(KK) = 0
C***********************************************************************
C     CHECK OF SPECIES CONTAINED IN THE MECHANISM
C***********************************************************************
C---- IS SYMBOL KK A BLANK ?
      IF(NSC.EQ.'        ')               GOTO 130
C---- IS SYMBOL KK IN SPECIES LIST OR THIRD BODY?
      DO 110 II=1,NS
      IF(NSC//REPEAT(' ',NFILL).EQ.NSYMB(II)) THEN
         MAT(KK) = II
            GOTO 130
      ENDIF
  110 CONTINUE
C---- IF WE ARE HERE SYMBOL IS INVALID
      GOTO 920
C***********************************************************************
C     CALCULATE STOICHIOMETRIC COEFFICIENT OF SYMBOL KK
C***********************************************************************
  130 CONTINUE
      IF(KK.GT.1) THEN
C---- RIGHT OF "=" SIGN SYMBOL KK MUST REPRESENT PRODUCT
      IF((NZS(KK-1).EQ.'=').OR.(NZS(KK-1).EQ.'>')) NLORGL=1
      ENDIF
      IF(NLORGL.EQ.0) ISTOE(KK)=-ISTOE(KK)
      IF(NLORGL.EQ.1) ISTOE(KK)= ISTOE(KK)
  190 CONTINUE
C***********************************************************************
C     CALCULATE MATRIX OF POINTERS
C***********************************************************************
C---- SET POINTERS TO ZERO
      DO 5220 LL=1,NS
 5220 NSTOE(LL)=0
C---- IF SY
      DO 5210 KK=1,6
      INDEX=MAT(KK)
      IF(INDEX.GT.0.AND.INDEX.LE.NS)   THEN
      NSTOE(INDEX)=NSTOE(INDEX)+ISTOE(KK)
      ENDIF
 5210 CONTINUE
C**********************************************************************C
      GOTO 700
  400 CONTINUE
      FINISH=.TRUE.
      GOTO 700
  700 CONTINUE
      RETURN
C**********************************************************************C
C     ERROR EXITS                                                      C
C**********************************************************************C
C900  WRITE(NFILE6,901)
C901  FORMAT('1',5X,' NO MECHANISM AVAILABLE (STRING "MECH" NOT FOUND)')
C     GOTO 999
 910  WRITE(NFILE6,911) 
 911  FORMAT('0',5X,'ERROR - INCORRECT INPUT OF REACTION ')
      GO TO 999
 920  WRITE(NFILE6,921) NSC
 921  FORMAT('0',5X,'UNKNOWN SPECIES -',A,'-IN THE FOLLOWING REACTION')
      GOTO 999
 999  STOP=.FALSE.
      RETURN
C**********************************************************************C
C     FORMAT STATEMENTS                                                C
C**********************************************************************C
  810 FORMAT(6(I2,1X,A8,A1))
C**********************************************************************C
C     END OF -INRVEC-                                                  C
C**********************************************************************C
      END
      SUBROUTINE NUMSYM(NUM,SYM)
C***********************************************************************
C
C     **************************************************************
C     *                                                            *
C     *   CHANGE INTEGER VALUES FROM 0 TO 9 IN TO SYMBOL           *
C     *                                                            *
C     **************************************************************
C
C***********************************************************************
C***********************************************************************
C     STORAGE ORGANIZATION, INITIAL VALUES
C***********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER*1 SYM,ZSYM(10)
      DATA ZSYM/' ','1','2','3','4','5','6','7','8','9'/
C***********************************************************************
C     ASSIGN SYMBOLS
C***********************************************************************
      IF(0.LE.NUM.AND.NUM.LE.9) THEN
      SYM=ZSYM(NUM+1)
      ELSE
      SYM='*'
      ENDIF
      RETURN
      END
      SUBROUTINE HOICSP(NH,HSYMB,NFILE2,NFILE6,NS,NSYMB,
     1    NCSMAX,NCS,ICS)
C**********************************************************************C
C                                                                      C
C    ***************************************************************   C
C    *                                                             *   C
C    *    PROGRAM FOR THE INPUT AND PRINTOUT OF SPECIES SYMBOLS    *   C
C    *                                                             *   C
C    *              LAST CHANGE: MAAS                              *   C
C    *                                                             *   C
C    ***************************************************************   C
C                                                                      C
C                                                                      C
C**********************************************************************C
C                                                                      C
C     INPUT  :                                                         C
C                                                                      C
C     NFILE2        = NUMBER OF INPUT FILE                             C
C     NFILE6        = NUMBER OF OUTPUT FILE FOR LISTING                C
C     NFILE3        = NUMBER OF OUTPUT FOR STORAGE                     C
C                                                                      C
C     OUTPUT :                                                         C
C                                                                      C
C     NCS           = NUMBER OF CONTROLLING SPECIES                    C
C     NSYMB(*)      = SPECIES SYMBOLS                                  C
C     ICS(NCS)      = INDICES OF CONTROLLING SPECIES                   C
C                                                                      C
C     INPUT FROM FILE NFILE2 :                                         C
C                                                                      C
C     SPECIES SYMBOLS IN THE FORMAT 14(A8,A1)                          C
C                                                                      C
C**********************************************************************C
C                                                                      C
C-Character only 8 characters allowed on input  
C                                                                      C
C**********************************************************************C
C     STORAGE ORGANIZATION AND DEFINITION OF INITIAL VALUES            C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL EQUSYM
      CHARACTER(LEN=*)  NSYMB(NS)
      CHARACTER*8 MMMM(7)
      CHARACTER*4 CHAR4,HSYMB(NH)
      CHARACTER*1 NNNN(7)
      DIMENSION ICS(NCSMAX)
C**********************************************************************C
C     INPUT OF THE HEADER                                              C
C**********************************************************************C
      LENSYM=LEN(NSYMB(1))
      NFILL = LENSYM-8
      REWIND NFILE2
      NCS=0
   81 FORMAT(A4)
   10 READ(NFILE2,81,END=900) CHAR4
      DO 15 I=1,NH
      IF(EQUSYM(4,CHAR4,HSYMB(I)))  GOTO 17
   15 CONTINUE
      GOTO 10
   17 CONTINUE
C**********************************************************************C
C     INPUT OF THE SPECIES SYMBOLS                                     C
C**********************************************************************C
   82 FORMAT(7(A8,A1))
  100 READ(NFILE2,82,END=910) (MMMM(J),NNNN(J),J=1,7)
      IF(EQUSYM(3,MMMM(1),'END')) GO TO 700
      DO 120 J=1,7
        IF(MMMM(J).EQ.'        ') GOTO 120
        IF(MMMM(J).EQ.'T       ')  THEN
            NCS=NCS+1
            IF(NCS.GT.NCSMAX) GO TO 920
            ICS(NCS)=0
            GOTO 120
          ENDIF
        DO 110 I=1,NS
          IF(MMMM(J)//REPEAT(' ',NFILL).EQ.NSYMB(I)) THEN
            NCS=NCS+1
            IF(NCS.GT.NCSMAX) GO TO 920
            ICS(NCS)=I
            GOTO 120
          ENDIF
  110   CONTINUE
        IQUA=J
        GOTO 930
  120 CONTINUE
      GOTO 100
C**********************************************************************C
C     PRINTOUT OF SPECIES SYMBOLS                                      C
C**********************************************************************C
  700 CONTINUE
C**********************************************************************C
C     regular exit                                                     C
C**********************************************************************C
      RETURN
C**********************************************************************C
C     IRREGULAR EXITS                                                  C
C**********************************************************************C
  900 CONTINUE
      WRITE(NFILE6,901)
  901 FORMAT('0  ','WARNING - No list of controlling species available')
      RETURN
  910 CONTINUE
      WRITE(NFILE6,911)
  911 FORMAT('0',5X,'ERROR - List of controlling species not complete ')
      GOTO 999
  920 CONTINUE
      WRITE(NFILE6,921)
  921 FORMAT('0',5X,'ERROR - Too many controlling species: ',
     1       ' NCS >  NCSMAX = ',I3)
      GOTO 999
  930 CONTINUE
      WRITE(NFILE6,931) MMMM(IQUA)
  931 FORMAT('0',5X,'ERROR - Unidentified species: ',A)
      GOTO 999
  999 CONTINUE
      WRITE(NFILE6,998)
  998 FORMAT(/,3(' ',' ++++++  Error in HOICSP  ++++++',/))
      STOP
C**********************************************************************C
C     END OF -HOICSP-                                                  C
C**********************************************************************C
      END
      SUBROUTINE SPLCON(LINE,SYMB,VALUE,SYM2,VAL2,IERR)
C***********************************************************************
C
C***********************************************************************
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      LOGICAL LTIM,LMIX
      CHARACTER(LEN=*) LINE 
      CHARACTER(LEN=*) SYMB,SYM2
C***********************************************************************
C
C***********************************************************************
      IERR = 0
      LTIM=.FALSE.
      LMIX =.FALSE.
      LINLEN=LEN(LINE)
      LENSY =LEN(SYMB)
      XNM = 0
      TIME = 0
      XMIX = 0
C***********************************************************************
C     Look for first delimiter and get symbol
C***********************************************************************
      IFD=SCAN(LINE,':=')
      IF(IFD.EQ.0) THEN
        WRITE(*,*) ' error, no delimiter found for species or keyword'
        IERR = 1
        GOTO 96
      ENDIF
      SYMB=LINE(1:IFD-1)
C---- replace symbol by blanks in LINE, then adjust left
      LINE(1:IFD)=REPEAT(' ',IFD)
      LINE=ADJUSTL(LINE)
C***********************************************************************
C     Read value and adjust left
C***********************************************************************
      IFB=SCAN(LINE,' ')
      READ(LINE(1:IFB-1),*) VALUE          
      LINE(1:IFB)=REPEAT(' ',IFB)
      LINE=ADJUSTL(LINE)
C***********************************************************************
C     check for next keyword   
C***********************************************************************
      IFD=SCAN(LINE,':=')
C---- 
      IF(IFD.EQ.0) THEN
        SYM2=' '
        SYM2=LINE
        VAL2 = 0
      ELSE IF(IFD.GT.1) THEN
        SYM2=LINE(1:IFD-1)
        LINE(1:IFD)=' '
        LINE=ADJUSTL(LINE)
        IS= SCAN(LINE,' ')-1
        IF(IS.LT.1) GOTO 94
        READ(LINE(1:IS),*,ERR=97) VAL2
      ELSE
         GOTO 95
      ENDIF 
      RETURN
C---- check for first blank 
   97 CONTINUE
      WRITE(*,*) ' Fatal error during input of a condition:',LINE
      IERR = 2
      RETURN
   96 WRITE(*,*) ' error, no delimiter found for species or keyword'
      IERR = 1
      RETURN 
   95 WRITE(*,*) ' error, delimiter found but not keyword       '
      IERR = 3
      RETURN 
   94 WRITE(*,*) ' delimiter and keyword forund, but no number  '
      IERR = 3
      RETURN 
      END
