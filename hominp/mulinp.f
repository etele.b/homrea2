      SUBROUTINE MULCON(NFIL10,NFILE6,NOPT,NSYMB,NS,NUR,MNZONE,XMOL,
     1           CHT1,CHT2,T,V,XU,TR,VIR,XIR,AFRAMU,ALPHA,FRACT,NEGR,
     1           WUEGR,TEGR,HCPL,HCPH,HINF) 
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *                                                           *    C
C     *    PROGRAM FOR THE INPUT OF MULTIZONE DATA FROM fort.2    *    C
C     *                                                           *    C
C     *            LAST CHANGE: 02.10.2005 / A.SCHUBERT           *    C
C     *                                                           *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C**********************************************************************C
C     I. STORAGE ORGANIZATION                                          C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      PARAMETER(LINLEN=255,LENOPT=8)
      CHARACTER(LEN=LINLEN) LINE
      DIMENSION NOPT(*),XIR(MNZONE,NS)
      DIMENSION HCPL(7,NS),HCPH(7,NS),HINF(3,NS)
      DOUBLE PRECISION, DIMENSION(NS):: XMOL,XU,WUEGR     
      DOUBLE PRECISION, DIMENSION(MNZONE):: TR,VIR,AFRAMU,FRACT,DMIR,
     1                                      NGVIR,NGMIR,XMOLM
      DOUBLE PRECISION, DIMENSION(MNZONE+1):: ALPHA     
C**********************************************************************C
C     II. LOGICAL, CHARACTER AND DIMENSION STATEMENTS                  C
C**********************************************************************C
      LOGICAL IERROR(10),CERROR,ERROR,EQUSYM,MASS
      CHARACTER(LEN=*)  NSYMB(NS)
      CHARACTER(LEN=LENOPT)  ISYM,NINP(10),SYM2
      COMMON/ENGDAT/ESPEED,DISP,COMRAT,ROD,BORE,STROKE,
     1              CABEG,CAEND,AFRATI,RON,EGRRAT,NFUEL 
C**********************************************************************C
      DATA ZERO/0.0D0/,ONE/1.0D0/,SMALL/1.0D-15/
      DATA NUINP/10/                   
      DATA NINP(1) /'MULTIZON'/,NINP(2)  /'REACTOR '/,
     1     NINP(3) /'V%      '/,NINP(4)  /'M%      '/,
     1     NINP(5) /'XXXXXXXX'/,NINP(6)  /'T       '/,
     1     NINP(7) /'LAMBDA  '/,NINP(8)  /'CHT     '/,   
     1     NINP(9) /'MIXING  '/,NINP(10) /'END     '/  
C*********************************************************************C
C     I.1: INITIALIZATION                                             C
C*********************************************************************C
      AFRAMU(1:MNZONE)   = ZERO
      FRACT(1:MNZONE)    = ZERO
      ALPHA(1:MNZONE+1)  = ZERO
      TR(1:MNZONE)       = ZERO
      XIR(1:MNZONE,1:NS) = ZERO
      VIR(1:MNZONE)      = ZERO
      DMIR(1:MNZONE)     = ZERO
      XMOLM(1:MNZONE)    = ZERO      
      NGVIR(1:MNZONE)    = 0
      NGMIR(1:MNZONE)    = 0      
      MIXNEW          = 0
      NFLAG           = 1
      IERROR(1:NUINP) = .FALSE.
      MASS            = .FALSE.
C*********************************************************************C
C-Character 
C*********************************************************************C
C     I.1: INITIALIZATION                                             C
C*********************************************************************C
      LENSY= LEN(NSYMB(1))
      NFILLB=LENSY-8           
C*********************************************************************C
C     I.1: INITIALIZATION                                             C
C*********************************************************************C
      IF(NOPT(4).NE.0) THEN      
        NUR = MAX(1,NUR)
      ENDIF      
      DO I=1,NUR      
        IF(NOPT(5).GE.2) THEN
CMag Adaptation to option engine 5
          if(nopt(5).ne.5) AFRAMU(I) = AFRATI
CMag end
          IF(NOPT(5).GE.3) FRACT(I) = EGRRAT
        ENDIF 
        XIR(I,1:NS) = XU(1:NS)      
        VIR(I)      = V / NUR
        IF (NUR.EQ.1) VIR(1) = 1.0
        TR(I)       = T  	     
        IF(NOPT(4).NE.0) THEN 
          IF(I.EQ.1.AND.NUR.EQ.1) THEN 
             ALPHA(I)    = CHT1            
             ALPHA(I+1)  = CHT2 	    
          ELSEIF(I.EQ.1) THEN
             ALPHA(I)    = CHT1
          ELSEIF (NUR.GT.1) THEN
             ALPHA(I)    = CHT2             
             ALPHA(I+1)  = CHT1
          ELSE
             ALPHA(I+1)  = CHT2
          ENDIF 
        ENDIF 
      ENDDO      
      IF (NOPT(32).LT.1) RETURN
C**********************************************************************C
C     I.2:  FIND MULTIZONE HEADING                                     C
C**********************************************************************C  
      IF(MNZONE.EQ.1) GOTO 1431
      REWIND NFIL10
  300 CONTINUE
      READ(NFIL10,'(A)',END=9000) LINE(1:LENOPT)
      IF (.NOT.(EQUSYM(6,LINE,NINP(1)))) THEN
        GOTO 300
      ELSE
        IERROR(1) = .TRUE.
      ENDIF
C**********************************************************************C
C     I.3: INPUT                                                       C
C**********************************************************************C
      L = 0
  301 CONTINUE
      READ(NFIL10,'(A)',END=9000) LINE
C---- END OF FILE        
      IF(EQUSYM(3,LINE,NINP(NUINP))) THEN
        IF(L.GT.NUR) THEN
          WRITE(NFILE6,9002)
          READ(*,*)
        ENDIF
C---- NUMBER OF SPECIES
        IF(IM.GT.NS) GOTO 9230         
        GOTO 333
      ENDIF      
      CALL SPLCON(LINE,ISYM,XNM,SYM2,YNM,IERR)
      IF(IERR.GT.0) GOTO 9100
      IF(EQUSYM(LENOPT,ISYM,NINP(2))) THEN
        L= NINT(XNM)
        IM    = 0
        DO M=2,NUINP-1
          IERROR(M) = .FALSE.       
        ENDDO 
C---- NUMBER OF SPECIES
        IF(IM.GT.NS) GOTO 9230 
        CERROR = .FALSE.	       
        IF (L .EQ. 0) THEN
          L = 1
        ENDIF     
        GOTO 301      
      ENDIF
C---- VOLUME REACTOR L      
      IF(.NOT.IERROR(3)) THEN
        IF(EQUSYM(LENOPT,ISYM,NINP(3))) THEN
          VIR(L)    = XNM              
          NGVIR(L)  = 1
          IERROR(3) = .TRUE.        	
          GOTO 301
        ENDIF	
      ENDIF      
C---- MASS REACTOR L      
      IF(.NOT.IERROR(4)) THEN
        IF(EQUSYM(LENOPT,ISYM,NINP(4))) THEN
          DMIR(L)   = XNM              
          NGVIR(L)  = 1
          NGMIR(L)  = 1
          IERROR(4) = .TRUE.        	
          IF (L.EQ.1) MASS = .TRUE.
          GOTO 301
        ENDIF	
      ENDIF  
C---- TEMPERATURE REACTOR L
      IF(.NOT.IERROR(6)) THEN
        IF(EQUSYM(LENOPT,ISYM,NINP(6))) THEN
          TR(L) = XNM              
          IERROR(6) = .TRUE.	
          GOTO 301         
        ENDIF     
      ENDIF      
C---- GAS COMPOSITION REACTOR L 
      IF(.NOT.IERROR(7)) THEN
        IF(EQUSYM(LENOPT,ISYM,NINP(7))) THEN
CMag Adaptation to option engine 5
          IF (NOPT(5).GE.2.and.nopt(5).ne.5) THEN
            AFRAMU(L) = XNM
C---- MOLE FRACTION CALCULATION
            CALL COMPO(NFUEL,AFRAMU(L),RON,XIR(L,1:NS),NS,NSYMB,
     1                 XMOL,NFILE6) 	    
            IERROR(7) = .TRUE.        
            GOTO 301
          ELSE
            WRITE(NFILE6,9004)
            STOP
          ENDIF        
        ELSE	
       DO J=1,NS
         IF(EQUSYM(LENSY,ISYM//REPEAT(' ',NFILLB),NSYMB(J))) THEN
              IF(.NOT.CERROR) XIR(L,1:NS)=ZERO
              XIR(L,J)= XNM	  
              IM=IM+1              
              CERROR = .TRUE.
              GOTO 301
            ENDIF
          ENDDO      
        ENDIF      
      ENDIF
C---- HEAT TRANSFER COEFFICIENT ALPHA [W/m**2 * K] REACTOR L
C---- USED FOR NEWTON APPROX.
      IF (.NOT.IERROR(8)) THEN
        IF(EQUSYM(LENOPT,ISYM,NINP(8))) THEN
          IF (NOPT(4).EQ.1.OR.NOPT(4).EQ.7) THEN
            ALPHA(L+1)= XNM
            IERROR(8) = .TRUE.
          ENDIF
          GOTO 301  
        ENDIF
      ENDIF 
C---- MIXING FUNCTION
      IF (.NOT.IERROR(9)) THEN
        IF(EQUSYM(LENOPT,ISYM,NINP(9))) THEN
          IF (NOPT(5).GE.3) THEN
            MIXNEW    = XNM
            IERROR(9) = .TRUE.
          ENDIF
          GOTO 301  
        ENDIF
      ENDIF           
C---- IF WE ARE HERE UNKNOWN SYMBOL IS DETECTED
      GOTO 9100
  333 CONTINUE  
C**********************************************************************C
C     I.4: NORMALIZATION OF INITIAL MIXTURE                            C
C**********************************************************************C
      IF (MASS) THEN
        NSGMIR = SUM(NGMIR(1:MNZONE)) 
        DO I=1,NUR
          DMIR(I) = NGMIR(I) * DMIR(I);     
        ENDDO
        DMREST = 1.0 - SUM(DMIR(1:NUR))
        DO I=1,NUR	
          IF(NGMIR(I).EQ.0) THEN
            DMIR(I) = DMREST/(NUR-NSGMIR)
          ENDIF      
          XMOLM(I) = DOT_PRODUCT(XMOL(1:NS),XIR(I,1:NS))
        ENDDO              	
        TEDXOM = ZERO
        DO I=1,NUR	
          TEDXOM = TEDXOM + DMIR(I) * (TR(I) / XMOLM(I))     
        ENDDO 
        DO I=1,NUR	
          VIR(I) = DMIR(I) * (TR(I) / XMOLM(I)) * (1 / TEDXOM)     
        ENDDO 
      ENDIF
C---- CHECK VOLUME
      NSGVIR = SUM(NGVIR(1:MNZONE)) 
      DO I=1,NUR
        VIR(I) = NGVIR(I) * VIR(I);     
      ENDDO
      VREST = 1.0 - SUM(VIR(1:NUR))
      DO I=1,NUR	
        IF(NGVIR(I).EQ.0) THEN
          VIR(I) = VREST/(NUR-NSGVIR)
        ENDIF      
      ENDDO      
      DO I=1,NUR       
        XSUM=SUM(XIR(I,1:NS))	     
        DO J=1,NS
          XIR(I,J)=XIR(I,J)/XSUM
        ENDDO
      ENDDO
C**********************************************************************C
C     I.5: MIXING WHEN NECESSARY                   
C**********************************************************************C      
      IF(NOPT(5) .EQ. 3) THEN 
        IF(NEGR.EQ.0) THEN       
          IF (MIXNEW.EQ.2) THEN
            CALL REDMIX(NS,MNZONE,NUR,FRACT,NFILE6)
          ENDIF        
          DO I=1,NUR         
            CALL MIXEGR(FRACT(I),XMOL,XIR(I,1:NS),TR(I),NS,
     1                  NSYMB,HCPL,HCPH,HINF,TEGR,WUEGR,NFILE6)
          ENDDO      
        ENDIF 
      ENDIF      
C**********************************************************************C
C     I.6: NORMALIZATION OF INITIAL VOLUME                             C
C**********************************************************************C
      VSUM = SUM(VIR(1:NUR))
      IF ((VSUM-ONE).GT.SMALL) THEN
       GOTO 9200
      ENDIF
      IF((1.0-VSUM).GT.SMALL) THEN   
       VSUM = ZERO
       DO I=1,NUR
         IF(VIR(I).LT.SMALL) J = I
         IF(VIR(I).GT.SMALL) THEN
           VSUM = VSUM + VIR(I)
         ENDIF
       ENDDO
       VIR(J) = 1.0 - VSUM
      ENDIF                
C**********************************************************************C
C      target of jump if only one zone                                 C
C**********************************************************************C
 1431 CONTINUE
C**********************************************************************C
C     IX. REGULAR EXIT                                                 C
C**********************************************************************C
      RETURN
C**********************************************************************C
C     ERROR EXITS                                                      C
C**********************************************************************C
 9100 WRITE(NFILE6,9101)ISYM
 9101 FORMAT(6X,'ERROR - WRONG SYMBOL IN MULZON CONDITIONS : ',
     1       2A4,'...')
      STOP
 9000 WRITE(NFILE6,9001)  
      STOP
 9200 WRITE(NFILE6,9005)
      STOP
 9230 WRITE(NFILE6,9231) NS
      STOP
C**********************************************************************C
C     VIII. FORMAT STATEMENTS                                          C
C**********************************************************************C
 9001 FORMAT(6X,'ERROR - NO MULZON CONDITIONS SPECIFIED')
 9002 FORMAT(6X,'ERROR - TOO MUCH MULZON CONDITIONS SPECIFIED')
 9004 FORMAT(6X,'ERROR - ENGINE OPTION GREATER/EQUAL 2 NEEDED !!!') 
 9005 FORMAT(6X,'ERROR - TOTAL VOLUME OF REACTORS EXCEEDS 100%')
 9231 FORMAT(6X,'ERROR - More than ',I2,' initial values defined')
C***********************************************************************
C     END OF -MULCON-
C***********************************************************************     
      END     

      SUBROUTINE MULOUT(NUR,NS,NOPT,MNZONE,TR,VIR,XIR,AFRAMU,ALPHA,
     1                  NFILE3)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *                                                           *    C
C     *     PROGRAM FOR THE OUTPUT OF MULTIZONE DATA TO fort.3    *    C
C     *                                                           *    C
C     *            LAST CHANGE: 02.10.2005 / A.SCHUBERT           *    C
C     *                                                           *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C**********************************************************************C
C     CHAPTER I: STORAGE ORGANIZATION                                  C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL EQUSYM
      DIMENSION NOPT(*)
      DOUBLE PRECISION, DIMENSION(MNZONE):: TR,VIR,AFRAMU
      DOUBLE PRECISION, DIMENSION(MNZONE+1):: ALPHA  
      DIMENSION XIR(MNZONE,NS)
C**********************************************************************C
C     WRITE ON NFILE3                                                  C
C**********************************************************************C
      IF (NOPT(32).GT.0) THEN
        WRITE(NFILE3,700)
        DO L=1,NUR
          WRITE(NFILE3,701) L      
          WRITE(NFILE3,702) TR(L)
          WRITE(NFILE3,704) (XIR(L,J),J=1,NS)
        ENDDO  
        WRITE(NFILE3,703) (VIR(L),L=1,NUR)
      ENDIF
      IF (NOPT(4).GT.0) THEN           
        WRITE(NFILE3,706) (ALPHA(L),L=1,NUR+1)
      ENDIF	     
      RETURN
C**********************************************************************C
C     VIII. FORMAT STATEMENTS                                          C
C**********************************************************************C
  700 FORMAT(1X,'MULTIZONE')
  701 FORMAT(1X,'REACTOR',I2,':')
  702 FORMAT(1X,'T(K)     : ',1PE11.4)
  703 FORMAT(1X,'V(%)',7X,5(1PE11.4,1X))  
  704 FORMAT(1X,'XU  ',7X,5(1PE11.4,1X))
  706 FORMAT(1X,'ALPHA',6X,5(1PE11.4,1X))
C***********************************************************************
C     END OF -MULOUT-                                                  *
C***********************************************************************
      END

      SUBROUTINE MULOU2(NUR,NS,NSYMB,MNZONE,XIR,PI,VI,VIR,TR,NFILE6,
     1                  AFRAMU,ALPHA,FRACT,NOPT,NPOP,NPOV,TIM)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *                                                           *    C
C     *     PROGRAM FOR THE OUTPUT OF MULTIZONE DATA TO NFILE6    *    C
C     *                                                           *    C
C     *            LAST CHANGE: 02.10.2005 / A.SCHUBERT           *    C
C     *                                                           *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C**********************************************************************C
C     CHAPTER I: STORAGE ORGANIZATION                                  C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER(LEN=31) MSYMB(NS)
      CHARACTER(LEN=*)  NSYMB(*)
      DIMENSION NOPT(*)
      DOUBLE PRECISION, DIMENSION(MNZONE):: TR,VIR,AFRAMU,FRACT
      DOUBLE PRECISION, DIMENSION(MNZONE+1):: ALPHA 
      DIMENSION XIR(MNZONE,NS),XIPR(MNZONE,NS)
      DIMENSION PI(*),VI(*),TIM(3,*)
C**********************************************************************C
C     WRITE ON NFILE6                                                  C
C**********************************************************************C
      LENSY=LEN(NSYMB(1))
C**********************************************************************C
C     WRITE ON NFILE6                                                  C
C**********************************************************************C
      WRITE(NFILE6,2)
      WRITE(NFILE6,664) (PI(J),TIM(1,J),J=1,NPOP)
      WRITE(NFILE6,665) (VI(J),TIM(2,J),J=1,NPOV)
C---- HEAT TRANSFER COEFFICIENT      
        IF(NOPT(4).GE.1) THEN	
          WRITE(NFILE6,1)
          WRITE(NFILE6,8)
          WRITE(NFILE6,9) 0,1,(ALPHA(1))
          WRITE(NFILE6,1)        
        ENDIF       
      DO L=1,NUR
        WRITE(NFILE6,3)
        WRITE(NFILE6,662)L
        WRITE(NFILE6,1)
CMag Adaptation to option engine 5
        IF(NOPT(5).GE.2 .and. nopt(5).ne.5) THEN
          WRITE(NFILE6,7) AFRAMU(L)  
        ELSE
C---- GAS COMPOSITION
          WRITE(NFILE6,4)
C---- EXTRACT NON-ZERO MOLE FRACTION FOR PRINT-OUT
          IPR=0
          DO 2828 I=1,NS
          IF(XIR(L,I).LE.0.0D0) GOTO 2828
            IPR=IPR+1
            MSYMB(IPR)(1:LENSY)=NSYMB(I)
            XIPR(L,IPR)   =XIR(L,I)
 2828   CONTINUE
C---- PRINT NON-ZERO MOLE FRACTIONS
          WRITE(NFILE6,663) (MSYMB(J)(1:LENSY),XIPR(L,J),J=1,IPR)
          WRITE(NFILE6,1)
        ENDIF
C---- VALUES OF T,P,V
        WRITE(NFILE6,5)
        WRITE(NFILE6,668) (VIR(L))
        WRITE(NFILE6,666) (TR(L))
        WRITE(NFILE6,1)
C---- HEAT TRANSFER COEFFICIENT      
        IF(NOPT(4).GE.1) THEN	
          WRITE(NFILE6,8)
          WRITE(NFILE6,9) L,L+1,(ALPHA(L+1))
          WRITE(NFILE6,1)        
        ENDIF        
C--- MIXING FRACTION
        IF(NOPT(5).GE.3) THEN	
          WRITE(NFILE6,10)(FRACT(L))
          WRITE(NFILE6,1)
        ENDIF        	      
      ENDDO
      WRITE(NFILE6,6)
      WRITE(NFILE6,3)
      WRITE(NFILE6,1)
      RETURN
C**********************************************************************C
C     FORMAT STATEMENTS                                          
C**********************************************************************C      
    1 FORMAT(1X)
    2 FORMAT(8X,'DATA FOR ALL REACTORS')
    3 FORMAT(8X,64('*'))
    4 FORMAT(8X,'GAS COMPOSITION (MOLE FRACTIONS)')
    5 FORMAT(8X,'VALUES OF T(K) AND V(frac.) ')    
    6 FORMAT(8X,'END DATA FOR REACTORS')    
    7 FORMAT(8X,'LAMBDA ',1PE9.2)  
    8 FORMAT(8X,'HEAT TRANSFER COEFFICIENT ')   
    9 FORMAT(8X,'Constant in Newtons law between Zone ',I2,' and ',
     1       I2,' =',E10.3)
   10 FORMAT(8X,'EGRRAT ',1PE9.2) 
  662 FORMAT(8X,'DATA FOR REACTOR',I2)
  663 FORMAT(1(8X,3(A,':',1PE9.2,3X)))
  664 FORMAT(8X,'P : ',1PE9.3,' bar      at t = ',1PE9.3,' s')  
  665 FORMAT(8X,'V : ',1PE9.3,' (-)      at t = ',1PE9.3,' s')
  666 FORMAT(8X,'T : ',1PE9.3,' K')
  668 FORMAT(8X,'V : ',1PE9.3,' (-)')  
C***********************************************************************
C     END OF -MULOU2-                                                  *
C***********************************************************************
      END

      SUBROUTINE WALCON(NFILE6,NFIL10,TSURR,SURVOL,CHT1,CHT2)
C**********************************************************************C
C                                                                      C
C     *************************************************************    C
C     *                                                           *    C
C     *                                                           *    C
C     *  PROGRAM FOR THE INPUT OF HEAT TRANSFER DATA FROM fort.2  *    C
C     *                                                           *    C
C     *            LAST CHANGE: 02.10.2005 / A.SCHUBERT           *    C
C     *                                                           *    C
C     *************************************************************    C
C                                                                      C
C**********************************************************************C
C**********************************************************************C
C     CHAPTER I: STORAGE ORGANIZATION                                  C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C**********************************************************************C
C     II. LOGICAL, CHARACTER AND DIMENSION STATEMENTS                  C
C**********************************************************************C
      LOGICAL IERROR(6),EQUSYM
      PARAMETER(LENOPT=8,LINLEN=255) 
      CHARACTER(LEN=LENOPT)  ISYM,NINP(6),SYM2
      CHARACTER(LEN=LINLEN)  LINE
C**********************************************************************C
      DATA ZERO/0.0D0/
      DATA NUINP/6/                   
      DATA NINP(1) /'HEATTRAN'/,NINP(2) /'S/V     '/,
     1     NINP(3) /'TSURR   '/,NINP(4) /'CHT1    '/,
     1     NINP(5) /'CHT2    '/,NINP(6) /'END     '/     
C*********************************************************************C
C     I.1: INITIALIZATION                                             C
C*********************************************************************C
      SURVOL = ZERO      
      TSURR  = ZERO
      CHT1   = ZERO
      CHT2   = ZERO    
      IERROR(1:NUINP) = .FALSE.
C**********************************************************************C
C     I.2:  FIND MULTIZONE HEADING                                     C
C**********************************************************************C  
      REWIND NFIL10
  300 CONTINUE
      READ(NFIL10,'(A)',END=9000) LINE
      IF (.NOT.(EQUSYM(4,LINE,NINP(1)))) THEN
        GOTO 300
      ELSE
        IERROR(1) = .TRUE.
      ENDIF
C**********************************************************************C
C     I.3: INPUT                                                       C
C**********************************************************************C
  301 CONTINUE
      READ(NFIL10,'(A)',END=9000) LINE
C---- END OF FILE      
      IF(EQUSYM(3,LINE,NINP(NUINP))) THEN        
        RETURN   
      ENDIF 
      CALL SPLCON(LINE,ISYM,XNM,SYM2,YNM,IERR)
      IF(IERR.GT.0) GOTO 9000
C---- SURFACE / VOLUME RATIO
      IF(.NOT.IERROR(2)) THEN
        IF(EQUSYM(LENOPT,ISYM,NINP(2))) THEN
          SURVOL    = XNM              
          IERROR(2) = .TRUE.	
          GOTO 301         
        ENDIF     
      ENDIF      
C---- WALL TEMPERATURE
      IF(.NOT.IERROR(3)) THEN
        IF(EQUSYM(LENOPT,ISYM,NINP(3))) THEN
          TSURR     = XNM              
          IERROR(3) = .TRUE.	
          GOTO 301         
        ENDIF     
      ENDIF   
C---- HEAT TRANSFER CONSTANT 1
      IF(.NOT.IERROR(4)) THEN
        IF(EQUSYM(LENOPT,ISYM,NINP(4))) THEN
          CHT1      = XNM              
          IERROR(4) = .TRUE.	
          GOTO 301         
        ENDIF     
      ENDIF   
C---- HEAT TRANSFER CONSTANT 2
      IF(.NOT.IERROR(5)) THEN
        IF(EQUSYM(LENOPT,ISYM,NINP(5))) THEN
          CHT2      = XNM              
          IERROR(5) = .TRUE.	
          GOTO 301         
        ENDIF     
      ENDIF               
C**********************************************************************C
C     ERROR EXITS                                                      C
C**********************************************************************C       
      WRITE(NFILE6,9101)
      STOP          
 9000 WRITE(NFILE6,9001)  
      STOP
C**********************************************************************C   
 9001 FORMAT(6X,'ERROR - NO HEAT TRANSFER CONDITIONS SPECIFIED')
 9101 FORMAT(6X,'ERROR - WRONG SYMBOL IN HEAT TRANSFER CONDITIONS')   
C***********************************************************************
C     END OF -WALCON-                                                  *
C***********************************************************************
      END



