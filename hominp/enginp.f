          
      SUBROUTINE ENGCON(NFIL10,NFILE6,NOPT,NSYMB,NS,XMOL,T0,XU,NEGR,
     1                  WUEGR,TEGR,PEGR,EGRM,HCPL,HCPH,HINF) 
C**********************************************************************C
C                                                                      
C     *************************************************************    
C     *                                                           
C     *                                                           
C     *  PROGRAM FOR THE INPUT OF ENGINE PARAMETERS FROM fort.2   
C     *                                                               
C     *            LAST CHANGE: 02.10.2005 / A.SCHUBERT               
C     *                                                               
C     *************************************************************    
C                                                                      
C**********************************************************************C
C**********************************************************************C
C     I. STORAGE ORGANIZATION                                          
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      COMMON/ENGDAT/ESPEED,DISP,COMRAT,ROD,BORE,STROKE,
     1              CABEG,CAEND,AFRATI,RON,EGRRAT,NFUEL   
      DIMENSION HCPL(7,NS),HCPH(7,NS),HINF(3,NS),XMOL(NS)      
      DIMENSION NOPT(*),XU(NS),WUEGR(NS)
C**********************************************************************C
C     II. LOGICAL, CHARACTER AND DIMENSION STATEMENTS                  
C**********************************************************************C
      LOGICAL IERROR(30),ERROR,EQUSYM
      CHARACTER(LEN=*)  NSYMB(NS)
      CHARACTER*17 ISYM,NINP(30),ENGSYM(30)
      CHARACTER*29 TEXT1
      CHARACTER*23 TEXT2    
      DIMENSION ENINP(30)
C**********************************************************************C
      DATA ZERO/0.0D0/,SMALL/1.0D-15/
      DATA LIMIT1/8/,LIMIT2/11/,LIMIT3/12/,LIMIT5/9/
      DATA GOT/-360/,NUINP/30/                   
      DATA NINP(1) /'ENGINESIMULATION '/,NINP(2) /'FOUR-STROKE      '/,
     1     NINP(3) /'DISPLACEMENT     '/,NINP(4) /'COMPRESSION-RATIO'/,
     1     NINP(5) /'CONNECTING-ROD   '/,NINP(6) /'BORE             '/,
     1     NINP(7) /'STROKE           '/,NINP(8) /'ENGINE-SPEED     '/,
     1     NINP(9) /'COMPUTATION-START'/,NINP(10)/'COMPUTATION-END  '/,
     1     NINP(11)/'LAMBDA           '/,NINP(12)/'FUEL             '/,
     1     NINP(13)/'RON              '/,NINP(14)/'EGR-RETAINMENT   '/,
     1     NINP(15)/'-----------------'/,NINP(16)/'-----------------'/,
     1     NINP(17)/'-----------------'/,NINP(18)/'-----------------'/,
     1     NINP(19)/'-----------------'/,NINP(20)/'-----------------'/,
     1     NINP(21)/'-----------------'/,NINP(22)/'-----------------'/,
     1     NINP(23)/'-----------------'/,NINP(24)/'-----------------'/,
     1     NINP(25)/'-----------------'/,NINP(26)/'-----------------'/,
     1     NINP(27)/'-----------------'/,NINP(28)/'-----------------'/,
     1     NINP(29)/'-----------------'/,NINP(30)/'END              '/
C*********************************************************************C
C     I.1: INITIALIZATION                                             
C*********************************************************************C
      DO M=1,NUINP
        ENINP(M)  = ZERO
        IERROR(M) = .FALSE.
      ENDDO
      WUEGR(1:NS) = ZERO
      TEGR = ZERO      
      PEGR = ZERO      
      EGRM = ZERO      
      NEGR = 0
      IF(NOPT(5) .EQ. 1) LIMIT = LIMIT1
      IF(NOPT(5) .EQ. 2) LIMIT = LIMIT2
      IF(NOPT(5) .EQ. 3) LIMIT = LIMIT3
CMag Option engine 5:
CMag Combination of option engine 1 and 3.
CMag Free choice of fuel composition combined with EGR-mixing.
CMag Number of input variables: limit1 + 1
      if(nopt(5).eq.5) limit = limit5
CMag end
C**********************************************************************C
C     I.2:  FIND ENGINE HEADING                                        
C**********************************************************************C  
      REWIND NFIL10
  300 READ(NFIL10,800,END=9000) ISYM
      IF (.NOT.(EQUSYM(16,ISYM,NINP(1)))) THEN
        GOTO 300
      ELSE
        IERROR(1) = .TRUE.
      ENDIF
C**********************************************************************C
C     I.3: INPUT                                                      
C**********************************************************************C
      LOOPNO = 0
  301 CONTINUE
      READ(NFIL10,801) ISYM,XNM     
      IF(EQUSYM(11,ISYM,NINP(2))) GOTO 301      
      DO L=1,NUINP
C------ END OF FILE
        IF(EQUSYM(3,ISYM,NINP(NUINP))) THEN
           GOTO 333
        ENDIF
        IF(EQUSYM(17,ISYM,NINP(L))) THEN
           ENINP(L)  = XNM
           ENGSYM(L) = ISYM
           IERROR(L) = .FALSE.
           LOOPNO    = LOOPNO + 1
           GOTO 301
        ENDIF
C------ CHECK IF THE SYMBOL IS REALLY UNKNOWN
        ERROR = .TRUE.
        DO K=1,NUINP
           IF(EQUSYM(17,ISYM,NINP(K))) ERROR = .FALSE.
        ENDDO
C------ IF WE ARE HERE AN UNKOWN INPUT SYMBOL WAS DETECTED !!!
        IF(ERROR .EQV. .TRUE.) GOTO 920
      ENDDO
  333 CONTINUE
C---- ASSIGN VALUES TO NEW VARIABLES
      DISP   = ENINP(3)
      COMRAT = ENINP(4)
      ROD    = ENINP(5)
      BORE   = ENINP(6)
      STROKE = ENINP(7)
      ESPEED = ENINP(8)
      CABEG  = ENINP(9)
      CAEND  = ENINP(10)
C---- FUEL      
      AFRATI = ENINP(11)
      NFUEL  = ENINP(12)
      RON    = ENINP(13)
      EGRRAT = ENINP(14)
CMag Input of EGR data 
CMag and determination of EGR-fuel-mixture initial conditions
      if(nopt(5).ne.5) then
        IF(NOPT(5).GE.2) THEN        
          CALL COMPO(NFUEL,AFRATI,RON,XU,NS,NSYMB,XMOL,NFILE6) 
        ENDIF            
        IF(NOPT(5).EQ.3) THEN        
          CALL REDEGR(NS,NSYMB,TEGR,PEGR,EGRM,WUEGR,NEGR,NFILE6)
          IF(NOPT(32).EQ.0) THEN
            CALL MIXEGR(EGRRAT,XMOL,XU,T0,NS,NSYMB,HCPL,HCPH,HINF,
     1                TEGR,WUEGR,NFILE6)
          ENDIF
        ENDIF
      else
        CALL REDEGR(NS,NSYMB,TEGR,PEGR,EGRM,WUEGR,NEGR,NFILE6)
          IF(NOPT(32).EQ.0) THEN
            CALL MIXEGR(EGRRAT,XMOL,XU,T0,NS,NSYMB,HCPL,HCPH,HINF,
     1                TEGR,WUEGR,NFILE6)
          ENDIF
      endif
CMag end
                  
C**********************************************************************C
C     VI. CHECK DATA                                                   
C**********************************************************************C
  400 CONTINUE
C---- CHECK COMPLETENESS OF DATA
      IF(LOOPNO .LT. LIMIT) GOTO 970

C**********************************************************************C
C     FOR THIS ENGINE OPTION GREATHER THAN 1 IS NEEDED                 
C**********************************************************************C
CMag Ron is not required in option engine 5
      if(nopt(5).ne.5) then
        IF(NOPT(5) .GT. 1) THEN
          IF(RON .LT. ZERO .OR. RON .GT. 100D0) GOTO 937
        ENDIF
      endif
CMag end
C**********************************************************************C
C     IX. REGULAR EXIT                                                 
C**********************************************************************C
      RETURN
C**********************************************************************C
C     VIII. FORMAT STATEMENTS                                          
C**********************************************************************C
  800 FORMAT(A17)
  801 FORMAT(A17,1X,1PE14.6)
  404 FORMAT(4X,A17,1X,'=',1PE10.3)
  990 FORMAT(15('+'),4X,A38,4X,15('+'))
  991 FORMAT(15('+'),4X,A18,2X,A17,4X,15('+'))
  992 FORMAT(15('+'),4X,A31,1X,I2,8X,15('+'))
  993 FORMAT(15('+'),4X,A17,3X,A17,5X,15('+'))  
 9001 FORMAT(1H ,5X,'ERROR - No engine conditions specified')
C**********************************************************************C
C     ERROR EXITS                                                      
C**********************************************************************C
  920 CONTINUE
      WRITE(NFILE6,990) '          ERROR in -ENGCON-           '
      WRITE(NFILE6,991) 'UNKNOWN INPUT DATA',ISYM
      STOP
  937 CONTINUE
      WRITE(NFILE6,995) '          ERROR in -ENGCON-           '
      WRITE(NFILE6,995) 'FALSE RON-INPUT  -->   0 <= RON <= 100'
      STOP
  970 CONTINUE
      MISDAT = LIMIT - LOOPNO
      WRITE(NFILE6,990) '  INPUT DATA FOR ENGINE COMPUTATION   '
      WRITE(NFILE6,992) '     NOT COMPLETE ---  MISSING ',MISDAT
      DO K=1,LIMIT
        IF(IERROR(K)) THEN
          WRITE(NFILE6,993) '     MISSING  -->',NINP(K)
        ENDIF
      ENDDO
      STOP
 9000 WRITE(NFILE6,9001)  
      STOP
  995 FORMAT(15('+'),4X,A35,4X,15('+'))
C***********************************************************************
C     END OF -ENGCON-
C***********************************************************************     
      END    
            
      SUBROUTINE ENGOUT(NFILE3,NFILE6,NOPT)
C**********************************************************************C
C                                                                      
C     *************************************************************    
C     *                                                               
C     *                                                               
C     *  PROGRAM FOR THE OUTPUT OF ENGINE PARAMETERS TO fort.3        
C     *                                                               
C     *            LAST CHANGE: 02.10.2005 / A.SCHUBERT               
C     *                                                               
C     *************************************************************    
C                                                                      
C**********************************************************************C
C**********************************************************************C
C     I. STORAGE ORGANIZATION                                          
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      COMMON/ENGDAT/ESPEED,DISP,COMRAT,ROD,BORE,STROKE,
     1              CABEG,CAEND,AFRATI,RON,EGRRAT,NFUEL
      DIMENSION NOPT(*)
      DATA ZERO/0.0D0/
C**********************************************************************C    
      WRITE(NFILE3,800)
      WRITE(NFILE3,801) NFUEL,ESPEED,CABEG,CAEND
      WRITE(NFILE3,802) DISP,COMRAT,ROD,BORE
      WRITE(NFILE3,803) STROKE,AFRATI,RON,ZERO      
C**********************************************************************C
      RETURN
C**********************************************************************C
C     VIII. FORMAT STATEMENTS                                          C
C**********************************************************************C
  800 FORMAT(1X,'ENGINE')
  801 FORMAT(1X,'NFUEL,SPEED,CABEG,CAEND',8X,I3,1X,3(1PE11.4,1X))
  802 FORMAT(1X,'DISP,COMRAT,ROD,BORE',3X,4(1PE11.4,1X))
  803 FORMAT(1X,'STROKE,LAMBDA,RON',6X,4(1PE11.4,1X))
C***********************************************************************
C     END OF -ENGOUT-
C***********************************************************************
      END  
       
      SUBROUTINE ENGOU2(NFILE6,NOPT)
C**********************************************************************C
C                                                                      
C     *************************************************************    
C     *                                                               
C     *                                                               
C     *  PROGRAM FOR THE OUTPUT OF ENGINE PARAMETERS TO fort.6        
C     *                                                               
C     *            LAST CHANGE: 02.10.2005 / A.SCHUBERT               
C     *                                                               
C     *************************************************************    
C                                                                      
C**********************************************************************C
C**********************************************************************C
C     I. STORAGE ORGANIZATION                                          
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      COMMON/ENGDAT/ESPEED,DISP,COMRAT,ROD,BORE,STROKE,
     1              CABEG,CAEND,AFRATI,RON,EGRRAT,NFUEL
      DIMENSION NOPT(*)
C**********************************************************************C    
      WRITE(NFILE6,1)
      WRITE(NFILE6,800)
      WRITE(NFILE6,1)
      WRITE(NFILE6,802) ESPEED
      WRITE(NFILE6,803) CABEG
      WRITE(NFILE6,804) CAEND
      WRITE(NFILE6,805) DISP      
      WRITE(NFILE6,806) COMRAT
      WRITE(NFILE6,807) ROD      
      WRITE(NFILE6,808) BORE
      WRITE(NFILE6,809) STROKE           
CMag Adapting output for option engine 5
      if(nopt(5).ne.5) then
        IF (NOPT(5).GE.2) THEN
          WRITE(NFILE6,801) NFUEL
          WRITE(NFILE6,810) AFRATI
          WRITE(NFILE6,811) RON
        ENDIF
      endif
CMag
      IF (NOPT(5).GE.3) THEN
        WRITE(NFILE6,812) EGRRAT      
      ENDIF
       
C**********************************************************************C
      RETURN
C**********************************************************************C
C     VIII. FORMAT STATEMENTS                                          
C**********************************************************************C
    1 FORMAT(1X)
  800 FORMAT(8X,'ENGINE PARAMETER')
  801 FORMAT(8X,'Fuel                    =',I3,
     1          5X,'1) iso-octane/n-heptane')
  802 FORMAT(8X,'Engine-speed            =',ES10.3,' min^(-1) ')
  803 FORMAT(8X,'Computation Start       =',ES10.3,' CA (deg) ')
  804 FORMAT(8X,'Computation End         =',ES10.3,' CA (deg) ')
  805 FORMAT(8X,'Displacement            =',ES10.3,' m^(3) ')
  806 FORMAT(8X,'Compression-ratio       =',ES10.3,' (-) ')
  807 FORMAT(8X,'Connecting-rod          =',ES10.3,'  m ')      
  808 FORMAT(8X,'Bore                    =',ES10.3,'  m ')
  809 FORMAT(8X,'Stroke                  =',ES10.3,'  m ')       
  810 FORMAT(8X,'Lambda                  =',ES10.3,' (-) ') 
  811 FORMAT(8X,'RON                     =',ES10.3,' (-) ') 
  812 FORMAT(8X,'EGR-RETAINMENT          =',ES10.3,' (-) ') 
C***********************************************************************
C     END OF -ENGOU2-
C***********************************************************************
      END        

      SUBROUTINE COMPO(NFUEL,AFRATI,RON,XU,NS,NSYMB,XMOL,NFILE6)
C**********************************************************************C
C                                                                      
C     *************************************************************    
C     *                                                               
C     *    CALCULATION OF AN INITIAL COMPOSITION OF FUEL AND          
C     *     AIR AS A FUNCTION OF THE AIR/FUEL-RATIO (AFRATI)          
C     *                                                               
C     *              (by O. Maiwald, 14.08.2001)                      
C     *                                                               
C     *************************************************************    
C                                                                      
C**********************************************************************C
C                                                                      
C     INPUT  :    RON    : fuel octane number [-]                      
C                 AFRATI : air/fuel-ratio  [-]                         
C                 NS     : number of species                           
C                 NSYMB  : species symbols                             
C                 NFUEL  : 1 = iso-octane/n-heptane                    
C                                                                      
C     OUTPUT :    XU(I) : species mole fractions of species I [-]      
C                                                                      
C     VARIABLES:  RHOOCT : density of iso-octane  [kg/m^3]             
C                 RHOHEP : density of n-heptane   [kg/m^3]             
C                                                                      
C                 BnOCT  : mole number of iso-octane  [mol]            
C                 BnHEP  : mole number of n-heptane  [mol]             
C                 BnFUEL : mole number of the fuel mixture  [mol]      
C                 BnAIR  : mole number of the air mixture  [mol]       
C                 BnO2ST : stoichiometric mole number of O2  [mol]     
C                 BnN2ST : stoichiometric mole number of N2  [mol]     
C                 BnO2CA : calc. mole numb. of O2 as a func. of AFRATI 
C                 BnN2CA : calc XMOL. mole numb. of N2 as a func.      
C                          of AFRATI                                   
C                                                                      
C                 xOCT   : mole number of iso-octane  [mol]            
C                 xHEP   : mole number of n-heptane  [mol]             
C                                                                      
C                 CATOM  : sum of C atoms in the fuel mixture          
C                 HATOM  : sum of C atoms in the fuel mixture          
C                 BPSI   : molar N/O ratio (78/21 for AIR)  [-]        
C                                                                      
C**********************************************************************C
C**********************************************************************C
C     CHAPTER I: STORAGE ORGANIZATION                                  
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION XU(NS),XMOL(NS),KFUEL(4) 
      LOGICAL IERR      
      CHARACTER(LEN=*) NSYMB(*)
      COMMON/LAMDAT/AIFUST
C**********************************************************************C
C     I.1: DATA STATEMENTS                                             
C**********************************************************************C
      DATA RHOOCT/700.0D0/,RHOHEP/680.0D0/
      DATA NFIL37/37/
      DATA ZERO/0.0D0/,ONE/1.0D0/,RTOL/1.0D-07/
C**********************************************************************C
C     CHAPTER II: INITIALISIERUNG                                      
C**********************************************************************C
      IERR   = .TRUE.
      ERR    = ZERO
C---- CHECK IF ISO-OCTANE / N-HEPTANE MIXTURE IS TRUE
      IF (NFUEL .EQ. 1) THEN
        IERR = .FALSE.
C**********************************************************************C
C     CHAPTER II: INITIALISIERUNG                                      
C**********************************************************************C
        BPSI     = 78.0/21.0
        XU(1:NS) = ZERO
C**********************************************************************C
C     II.1: DETERMINE SPECIES NUMBERS                             
C**********************************************************************C
        CALL GESPNU(NFUEL,NS,NSYMB,KFUEL,NFILE6)
C**********************************************************************C
C     CHAPTER III: COMPUTATION                                         
C**********************************************************************C
C     III.1: MOLE NUMBERS AND MOLE FRACTION OF IC8H18 AND NC7H16       
C**********************************************************************C
C---- MOLE NUMBERS
        IF(RON.GT.ZERO.AND.RON.LT.1.0D2) THEN
          HELP1=(XMOL(KFUEL(3))/XMOL(KFUEL(4)))*(RHOHEP/RHOOCT)
     1          *(1.0D2/RON-1.0D0)+ONE
          IF(HELP1 .LE. ZERO) GOTO 910
          BNOCT  = ONE / HELP1
          BNHEP  = ONE - BNOCT
          IF(RON .GT. ZERO .AND. BNOCT .LE. ZERO) GOTO 910
        ELSEIF (RON.LT.RTOL) THEN
          BNOCT  = ZERO
          BNHEP  = ONE
        ELSEIF (RON.GT.(1.0D2-RTOL)) THEN
          BNOCT  = ONE
          BNHEP  = ZERO
        ENDIF	  	  	
C---- ASSUME BNFUEL = 1 MOL
        BNFUEL = BNOCT + BNHEP
C---- CHECK RESULT
        ERR = (BNFUEL-ONE)/ONE
        IF(ERR .GT. RTOL) GOTO 920
C---- MOLE FRACTIONS OF FUEL COMPOUNDS IN FUEL MIXTURE
        XOCT = BNOCT/BNFUEL
        XHEP = BNHEP/BNFUEL
C**********************************************************************C
C     III.2: CALCULATION OF STOICHIOMETRIC MOLE NUMBER OF O2 AND N2    C
C   >>>> CAUTION:  MOLE NUMBER OF FUEL MIX. SET EQUAL TO 1 MOL !  <<<< C
C   (see:  John B.  Heywood "Internal Combustion Engine Fundamentals") C
C**********************************************************************C
C---- NUMBER OF C AND H-ATOMS IN FUEL MIXTURE
        CATOM = XOCT*8  + XHEP*7
        HATOM = XOCT*18 + XHEP*16
C---- STOICHIOMETRIC MOLE NUMBERS OF AIR FOR 1 MOL FUEL MIXTURE
C----             (AIR-FUEL RATIO (AFRATI) =1)
        BNO2ST = CATOM  + HATOM/4
        BNN2ST = BNO2ST * BPSI
        BNCOMP = BNO2ST + BNN2ST + BNOCT + BNHEP
C---- MOLE NUMBERS
        IF(RON.GT.ZERO.AND.RON.LT.1.0D2) THEN
        XMOLM  = BNO2ST/BNCOMP * XMOL(KFUEL(1)) + BNN2ST/BNCOMP 
     1      * XMOL(KFUEL(2)) + BNOCT/BNCOMP * XMOL(KFUEL(3))
     1      + BNHEP/BNCOMP * XMOL(KFUEL(4))
        AIFUST = ((BNO2ST/BNCOMP*XMOL(KFUEL(1))/XMOLM)+(BNN2ST/BNCOMP
     1       *XMOL(KFUEL(2))/XMOLM)) / ((BNOCT/BNCOMP*XMOL(KFUEL(3))/
     1       XMOLM)+(BNHEP/BNCOMP*XMOL(KFUEL(4))/XMOLM))
        ELSEIF (RON.LT.RTOL) THEN
        XMOLM  = BNO2ST/BNCOMP * XMOL(KFUEL(1)) + BNN2ST/BNCOMP 
     1      * XMOL(KFUEL(2)) + BNOCT/BNCOMP * ZERO
     1      + BNHEP/BNCOMP * XMOL(KFUEL(4))
        AIFUST = ((BNO2ST/BNCOMP*XMOL(KFUEL(1))/XMOLM)+(BNN2ST/BNCOMP
     1       *XMOL(KFUEL(2))/XMOLM)) / ((BNOCT/BNCOMP*ZERO/
     1       XMOLM)+(BNHEP/BNCOMP*XMOL(KFUEL(4))/XMOLM))
        ELSEIF (RON.GT.(1.0D2-RTOL)) THEN
        XMOLM  = BNO2ST/BNCOMP * XMOL(KFUEL(1)) + BNN2ST/BNCOMP 
     1      * XMOL(KFUEL(2)) + BNOCT/BNCOMP * XMOL(KFUEL(3))
     1      + BNHEP/BNCOMP * ZERO
        AIFUST = ((BNO2ST/BNCOMP*XMOL(KFUEL(1))/XMOLM)+(BNN2ST/BNCOMP
     1       *XMOL(KFUEL(2))/XMOLM)) / ((BNOCT/BNCOMP*XMOL(KFUEL(3))/
     1       XMOLM)+(BNHEP/BNCOMP*ZERO/XMOLM))
        ENDIF			
C**********************************************************************C
C     III.3: MOLE NUMBER OF O2 AND N2 FOR INPUT AIR/FUEL-RATIO (AFRATI)
C**********************************************************************C
        BNO2CA = BNO2ST * AFRATI
        BNN2CA = BPSI * BNO2CA
        BNAIR  = BNO2CA + BNN2CA
C---- CHECK
        IF((BNN2CA-BNN2ST*AFRATI)/BNN2CA .GT. RTOL) GOTO 940
C**********************************************************************C
C     III.4: MOLE FRACTIONS IN THE AIR-FUEL-MIXTURE                    
C**********************************************************************C
        XO2  = BNO2CA / (BNFUEL + BNAIR)
        XN2  = BNN2CA / (BNFUEL + BNAIR)
        XOCT = BNOCT  / (BNFUEL + BNAIR)
        XHEP = BNHEP  / (BNFUEL + BNAIR)
C**********************************************************************C
C     III.5: UPDATE XU-VECTOR WITH CALC. AIR-FUEL-MIXTURE COMPOSITION  
C**********************************************************************C
        IF(RON.GT.ZERO.AND.RON.LT.1.0D2) THEN
          XU(KFUEL(3)) = XOCT
          XU(KFUEL(4)) = XHEP
          XU(KFUEL(1)) = XO2
          XU(KFUEL(2)) = XN2
        ELSEIF (RON.LT.RTOL) THEN
          XU(KFUEL(4)) = XHEP
          XU(KFUEL(1)) = XO2
          XU(KFUEL(2)) = XN2
        ELSEIF (RON.GT.(1.0D2-RTOL)) THEN
          XU(KFUEL(3)) = XOCT
          XU(KFUEL(1)) = XO2
          XU(KFUEL(2)) = XN2
        ENDIF
C---- CHECK IF SUMMATION OF THE MOLE FRACTION EQUAL TO 1
        XUSUM  = ZERO
        DO L=1,NS
          XUSUM = XUSUM + XU(L)
        ENDDO
        ERR = ZERO
        ERR = (XUSUM-ONE)/ONE
        IF(ABS(ERR) .GT. RTOL) GOTO 930
C**********************************************************************C
C     CHAPTER IV: OUTPUT MOLE NUMBERS TO 'fort.37'                                  
C**********************************************************************C
        IF (2.EQ.1) THEN
          OPEN(UNIT=NFIL37,FILE='fort.37')
          WRITE(NFIL37,81) 'INITIAL AIR-FUEL-MIXTURE (MOLE NUMBERS) 
     1                      FOR LAMBDA =', AFRATI
          WRITE(NFIL37,82) 'C8H18  :',BNOCT
          WRITE(NFIL37,82) 'C7H16  :',BNHEP
!           WRITE(NFIL37,82) 'IC8H18  :',BNOCT
!           WRITE(NFIL37,82) 'NC7H16  :',BNHEP
          WRITE(NFIL37,82) 'O2      :',BNO2CA
          WRITE(NFIL37,82) 'N2      :',BNN2CA
        ENDIF
      ENDIF
      IF(IERR) GOTO 900
C**********************************************************************C
C     CHAPTER V: FORMAT STATEMENTS                                     
C**********************************************************************C
   81 FORMAT(1X,A52,1PE10.3)
   82 FORMAT(4X,A9,1PE12.4)
C**********************************************************************C
C     CHAPTER VI: REGULAR EXIT                                         
C**********************************************************************C
      RETURN
C**********************************************************************C
C     CHAPTER VII: ERROR EXIT                                          
C**********************************************************************C
  900 CONTINUE
      WRITE(NFILE6,941) '    ERROR in SUBROUTINE -COMPO- !!!    '
      WRITE(NFILE6,941) ' IC8H18-NC7H16 NOT SPECIFIED IN FORT.2 '
CMag Additional information on error
      write(*,*)'++++ Check species names in enginp.f ++++'
      write(*,*)'++++ Subroutine gespnu ++++'
      STOP
  910 CONTINUE
      WRITE(NFILE6,941) '    ERROR in SUBROUTINE -COMPO- !!!    '
      STOP
  920 CONTINUE
      WRITE(NFILE6,941) '    ERROR in SUBROUTINE -COMPO- !!!    '
      WRITE(NFILE6,941) 'FUEL MOLE NUMBER COMPUTATION FAILED !!!'
      STOP
  930 CONTINUE
      WRITE(NFILE6,941) '    ERROR in SUBROUTINE -COMPO- !!!    '
      WRITE(NFILE6,941) '  SUMMATION OF MOLE FRACTION NOT 1.0   '
      STOP
  940 CONTINUE
      WRITE(NFILE6,941) '    ERROR in SUBROUTINE -COMPO- !!!    '
      WRITE(NFILE6,941) 'CALC. OF FRESH MIX. COMPOSITION FAILED '
      STOP
  941 FORMAT(7('+'),3X,A39,2X,7('+'))
C***********************************************************************
C     END OF -COMPO-
C***********************************************************************
      END  

      SUBROUTINE GESPNU(NFUEL,NS,NSYMB,KFUEL,NFILE6)
C**********************************************************************C
C                                                                      
C     *************************************************************    
C     *                                                               
C     *   GET SPECIES NUMBERS FROM THE INITIAL FUEL AIR MIXTURE       
C     *                                                               
C     *              (by A.Schubert, 07.11.2005)                      
C     *                                                               
C     *************************************************************    
C                                                                      
C**********************************************************************C
C                                                                      
C     INPUT  :    NS     : number of species                           
C                 NSYMB  : species symbols                             
C                 NFUEL  : 1 = iso-octane/n-heptane                    
C**********************************************************************C
C**********************************************************************C
C     CHAPTER I: STORAGE ORGANIZATION                                  
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION KFUEL(4) 
      CHARACTER(LEN=*)  NSYMB(*)
C**********************************************************************C
C     I.1: DATA STATEMENTS                                             C
C**********************************************************************C
      KFUEL(1:4) = 0
      NFILL=LEN(NSYMB(1))-8
C**********************************************************************C
C     II.1: DETERMINE SPECIES NUMBERS                               
C**********************************************************************C      
      IF (NFUEL .EQ. 1) THEN
        DO K=1,NS
          IF(NSYMB(K) .EQ. 'O2      '//REPEAT(' ',NFILL)) KFUEL(1)=K
          IF(NSYMB(K) .EQ. 'N2      '//REPEAT(' ',NFILL)) KFUEL(2)=K
!         IF(NSYMB(K) .EQ. 'C8H18   '//REPEAT(' ',NFILL)) KFUEL(3)=K
!         IF(NSYMB(K) .EQ. 'C7H16   '//REPEAT(' ',NFILL)) KFUEL(4)=K	  
          IF(NSYMB(K) .EQ. 'IC8H18  '//REPEAT(' ',NFILL)) KFUEL(3)=K
          IF(NSYMB(K) .EQ. 'NC7H16  '//REPEAT(' ',NFILL)) KFUEL(4)=K
        ENDDO
      ENDIF
C***********************************************************************
C     END OF -GESPNU-
C***********************************************************************
      END  

      SUBROUTINE REDEGR(NS,NSYMB,TEGR,PEGR,EGRM,WUEGR,NEGR,NFILE6)
C**********************************************************************C
C                                                                      
C     ************************************************************     
C     *                                                              
C     *          INPUT OF THE EGR-COMPOSTION FROM 'fort.32'           
C     *                                                               
C     *                (by A.Schubert / 20.11.2005)                   
C     *                                                               
C     ************************************************************     
C                                                                      
C**********************************************************************C
C                                                                      
C      INPUT:   NS      : number of species                            
C               NSYMB   : species symbols                              
C                                                                      
C      OUTPUT:  WUEGR(I): EGR mole fractions of species I              
C               TEGR    : temperature of exhaust gas mixture           
C               PEGR    : pressure of exhaust gas mixture              
C               EGRM    : mass of exhaust gas mixture                  
C                                                                      
C**********************************************************************C
C**********************************************************************C
C     I. STORAGE ORGANIZATION                                          
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C**********************************************************************C
C     II. CHARACTER,COMMON AND DIMENSION STATEMENTS                    
C**********************************************************************C
      PARAMETER(LENCIN=31,LINLEN=255)
      CHARACTER(LEN=*)  NSYMB(NS)
      CHARACTER(LEN=LINLEN)  LINE       
      CHARACTER(LEN=LENCIN)  ISYM
      CHARACTER(LEN=LENCIN)  SYM2
      CHARACTER*1 CONTUE
      DIMENSION WU(NS),WUEGR(NS)
C**********************************************************************C
C     III. DATA STATEMENTS                                             
C**********************************************************************C
      DATA NFIL32/32/,ZERO/0.0D0/,ONE/1.0D0/
C**********************************************************************C
C     IV. INITIALIZATION                                               
C**********************************************************************C
        PEGR  = ZERO
        TEGR  = ZERO
        EGRM  = ZERO
        WUEGR(1:NS) = ZERO
        LENSY=LEN(NSYMB(L))
C**********************************************************************C
C     V. CREATE EXHAUST GAS COMPOSITION VECTOR (WUEGR(NS))             
C**********************************************************************C
        OPEN(UNIT=NFIL32,FILE='fort.32',POSITION='REWIND')
  301   CONTINUE
        EGRSPE  = ZERO
        NDETECT = 0
        ISYM(1:LENCIN) = REPEAT(' ',LENCIN)
C**********************************************************************C
C       READ Line
C**********************************************************************C
      READ(NFIL32,'(A)',ERR=830) LINE              
      IF(LINE(1:8).EQ. '>Exhaust') GOTO 301
      IF(LINE(1:1).EQ. '<') GOTO 600
C**********************************************************************C
C       process   
C**********************************************************************C
      CALL SPLCON(LINE,ISYM,EGRSPE,SYM2,YNM,IERR)
      WRITE(*,*) ISYM(1:16),EGRSPE
      IF(IERR.GT.0) GOTO 820 
C**********************************************************************C
C     V. CREATE EXHAUST GAS COMPOSITION VECTOR (WUEGR(NS))             
C**********************************************************************C
        IF(ISYM(1:8).EQ. 'T(egr)  ') THEN
          TEGR = EGRSPE               
          GOTO 301
        ENDIF
        IF(ISYM(1:8).EQ. 'p(egr)  ') THEN
          PEGR = EGRSPE        
          GOTO 301
        ENDIF
        IF(ISYM(1:8).EQ. 'm(egr)  ') THEN
          EGRM = EGRSPE 
          GOTO 301
        ENDIF      
C---- BUILD EGR SPECIES MOLE FRACTIONS VECTOR IN RIGHT ORDER 
        DO L=1,NS
          IF (ISYM(1:LENSY).EQ. NSYMB(L)) THEN
            WUEGR(L) = EGRSPE
            NDETECT  = 1
          ENDIF
        ENDDO
C---- MAYBE AN UNKOWN EXHAUST SPECIES SYMBOL IS DETECTED 
        IF (NDETECT .EQ. 0) GOTO 810
        GOTO 301
  600 CONTINUE 
C---- NEW NORMALIZATION NEEDED 
      WUSUM = ZERO
      DO L=1,NS
        WUSUM = WUSUM + WUEGR(L)        
      ENDDO
      WUEGR(1:NS) = WUEGR(1:NS) / WUSUM
C**********************************************************************C
C     VIII. REGULAR EXIT                                               
C**********************************************************************C
  700 CONTINUE
      RETURN
C**********************************************************************C
C     IX. ERROR EXITS                                                  
C**********************************************************************C
  810 CONTINUE
      WRITE(NFILE6,811) 'ERROR in EXHAUST INPUT FILE (-REDEGR-) !!!  '
      WRITE(NFILE6,812) 'UNKNOWN EXHAUST SYMBOL - ',ISYM,' - DETEKTED'
      STOP
  820 CONTINUE
      WRITE(NFILE6,811) 'ERROR in EXHAUST INPUT FILE (-REDEGR-) !!!  '
      STOP
  830 CONTINUE
      WRITE(NFILE6,831) 'WARNING - NO EGR INPUT FILE FOUND !!!'
      WRITE(NFILE6,831) ' using initial composition of HOMREA '
      WRITE(NFILE6,831) 'STANDARD INPUT FILE as EGR gasmixture'
      WRITE(NFILE6,831) ' '
      NEGR = 1
      WRITE(NFILE6,832) '--> PRESS "y" AND RETURN TO CONTINUE'
      READ(*,*) CONTUE
      IF (CONTUE .EQ. 'N' .OR. CONTUE .EQ. 'n') THEN
          WRITE(NFILE6,833) '   PROGRAM STOPPED BY USER   '
          STOP         
      ENDIF
      IF (CONTUE .EQ. 'Y' .OR. CONTUE .EQ. 'y') THEN  
        GOTO 700
      ELSE 
        WRITE(NFILE6,833) 'WRONG INPUT - PROGRAM STOPPED'
        STOP
      ENDIF
C**********************************************************************C
C     X. ERROR FORMAT STATEMENTS                                       
C**********************************************************************C
  811 FORMAT(13('+'),4X,A44,4X,13('+'))
  812 FORMAT(13('+'),4X,A25,A,A11,4X,13('+'))
  821 FORMAT(13('+'),4X,A42,4X,13('+'))
  822 FORMAT(13('+'),2X,A24,1PE9.3,1X,A12,2X,13('+'))
  831 FORMAT(10('+'),3X,A37,3X,15('+'))
  832 FORMAT(9X,A36)
  833 FORMAT(10('+'),7X,A29,7X,10('+'))
C**********************************************************************C
C     END OF -REDEGR-                                                  
C**********************************************************************C
      END

      SUBROUTINE  MIXEGR(EGRRAT,XMOL,XU,T0,NS,NSYMB,
     1                   HCPL,HCPH,HINF,TEGR,WUEGR,NFILE6)
C**********************************************************************C
C                                                                      
C     *************************************************************    
C     *                                                           
C     *      MIXING FROM TWO MIXTURES WITH MASS FRACTIONS          
C     *                 (by A. Schubert, 28.10.2005)              
C     *                                                          
C     *************************************************************    
C                                                                      
C**********************************************************************C
C     I. STORAGE ORGANIZATION                                          C
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      CHARACTER(LEN=*) NSYMB(*)
      DIMENSION HMOL(NS),W0(NS)      
      DIMENSION XUEGR(NS),WUEGR(NS)     
      DIMENSION XU(NS),WFRE(NS),CPI(NS)        
      DIMENSION HCPL(7,NS),HCPH(7,NS),HINF(3,NS),XMOL(NS)
      DIMENSION HLHH(7,NS*2)
      DATA ZERO/0.0D0/
C**********************************************************************C
C   INITIALIZATION                                               
C**********************************************************************C
      WIFRME = ZERO
      XMOEGR = ZERO
      XMOLM  = ZERO
      XUSUM  = ZERO      
      DO I=1,NS      
        HLHH(1:7,I)    = HCPL(1:7,I)
        HLHH(1:7,I+NS) = HCPH(1:7,I)
      ENDDO
C**********************************************************************C
C   FRESH 
C**********************************************************************C
C---- TRANSFORM MOLE FRACTIONS INTO MASS FRACTIONS       
      DO L=1,NS
        WIFRME = WIFRME + (XU(L) * XMOL(L))
      ENDDO
      DO L=1,NS      
        WFRE(L) = (XU(L)*XMOL(L))/WIFRME
      ENDDO      
C---- calculate molar enthalpy	        
      CALL CALHCP(NS,XU,T0,HLHH,HINF,CPI,CPTOT,HMOL,IEHCP)
C---- mean molar mass
      XMOLM = DOT_PRODUCT(XMOL(1:NS),XU(1:NS))
C---- specific enthalpy
      HFRE = DOT_PRODUCT(XU(1:NS),HMOL(1:NS))/XMOLM
C**********************************************************************C
C   EGR
C**********************************************************************C
C---- mean molar mass EGR
      DO J=1,NS
        XMOEGR  = XMOEGR  + WUEGR(J)/XMOL(J)
      ENDDO	 
      XMOEGR = 1 / XMOEGR
C---- mole fractions
      XUEGR(1:NS) = WUEGR(1:NS) * XMOEGR / XMOL(1:NS)         
C---- calculate molar enthalpy
      CALL CALHCP(NS,XUEGR,TEGR,HLHH,HINF,CPI,CPTOT,HMOL,IEHCP)
C---- mean molar mass
      XMOLM = DOT_PRODUCT(XMOL(1:NS),XUEGR(1:NS))    
C---- specific enthalpy
      HEGR = DOT_PRODUCT(XUEGR(1:NS),HMOL(1:NS)) / XMOLM
C**********************************************************************C
C   MIXTURE
C**********************************************************************C
      W0(1:NS) = (1-EGRRAT) * WFRE(1:NS) + EGRRAT * WUEGR(1:NS)     
C---- transform into mole fractions      
      DO J=1,NS
        XMOLM  = XMOLM  + W0(J)/XMOL(J)
      ENDDO	       
      DO L = 1,NS	 
        XU(L) = (W0(L)/XMOL(L))/XMOLM
      ENDDO	 
      DO L = 1,NS      
        XUSUM = XUSUM + XU(L)
      ENDDO      
      XU(1:NS) = XU(1:NS)/XUSUM
C---- total specific enthalpy        			
      H0 = (1-EGRRAT) * HFRE + EGRRAT * HEGR        
C---- get temperature 
      CALL GETT(NS,H0,XU,HCPL,HCPH,HINF,XMOL,T0,NFILE6)
C**********************************************************************C
C     END OF -MIXEGR-                                                  
C**********************************************************************C
      END     

      SUBROUTINE REDMIX(NS,MNZONE,NUR,FRACT,NFILE6)
C**********************************************************************C
C                                                                      
C     ************************************************************     
C     *                                                          
C     *          INPUT OF THE EGR-FRACTION FROM 'fort.33'   
C     *                                                       
C     *                (by A.Schubert / 20.11.2005)             
C     *                                                        
C     ************************************************************     
C                                                                      
C**********************************************************************C
C**********************************************************************C
C     I. STORAGE ORGANIZATION                                          
C**********************************************************************C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C**********************************************************************C
C     II. CHARACTER,COMMON AND DIMENSION STATEMENTS                    
C**********************************************************************C
      DOUBLE PRECISION, DIMENSION(MNZONE):: FRACT
C**********************************************************************C
C     III. DATA STATEMENTS                                             
C**********************************************************************C
      DATA NFIL33/33/,ZERO/0.0D0/,ONE/1.0D0/
C**********************************************************************C
C     IV. INITIALIZATION                                               
C**********************************************************************C
        FRACT(1:MNZONE) = ZERO
C**********************************************************************C
C 
C**********************************************************************C
        OPEN(UNIT=NFIL33,FILE='fort.33',POSITION='REWIND')
        READ(NFIL33,1,ERR=810) NUMREA
        IF(NUMREA.EQ.0) GOTO 810
        IF(NUMREA.NE.NUR) GOTO 820        
        DO L=1,NUMREA
          READ(NFIL33,2) FRACT(L)
        ENDDO
C**********************************************************************C
C     VII. FORMAT STATEMENTS                                           
C**********************************************************************C
    1 FORMAT(I4)
    2 FORMAT(1X,1PE12.4)
C**********************************************************************C
C     VIII. REGULAR EXIT                                               
C**********************************************************************C
      RETURN
C**********************************************************************C
C     IX. ERROR EXITS                                                  
C**********************************************************************C
  810 CONTINUE
      WRITE(NFILE6,811) 'ERROR in FRACTION INPUT FILE (-REDMIX-) !!! '
      STOP
  820 CONTINUE
      WRITE(NFILE6,811) '  NOT ENOUGH ZONES DEFINED (-REDMIX-) !!!   '
      STOP
C**********************************************************************C
C     X. ERROR FORMAT STATEMENTS                                       
C**********************************************************************C
  811 FORMAT(13('+'),4X,A44,4X,13('+'))
C**********************************************************************C
C     END OF -REDMIX-                                                  
C**********************************************************************C
      END
